## Donorator

**Donorator** is a web application to manage information about donors and donations.

### Features:
- Edit donor's information: name, address, comments
- Print donor's information on the Avery labels
- Record donations
- Reports

### Requirements:
- PHP 8.2+
- MySQL
- PHP extensions:  
  `sudo apt install php-zip php-xml php-gd`  
  or equivalent in your OS
- PHP extension recommended:
  `sudo apt install php-imagick`

### Example of Nginx config:
```
server {
    server_name  www.site;

    root   /var/www/html/public;

    add_header X-Frame-Options "SAMEORIGIN";
    add_header X-XSS-Protection "1; mode=block";
    add_header X-Content-Type-Options "nosniff";

    index index.html index.htm index.php;

    location / {
        try_files $uri $uri/ /index.php?$query_string;
    }

    location = /favicon.ico { access_log off; log_not_found off; }
    location = /robots.txt  { access_log off; log_not_found off; }

    error_page 404 /index.php;

    location ~ \.php$ {
      try_files $uri =404;
      fastcgi_split_path_info ^(.+\.php)(/.+)$;
      fastcgi_pass unix:/run/php/php7.2-fpm.sock;
      fastcgi_index index.php;
      include       /etc/nginx/fastcgi.conf;
      fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
      fastcgi_param PATH_INFO $fastcgi_path_info;
    }

    # deny access to .htaccess files, if Apache's document root
    # concurs with nginx's one
    location ~ /\.ht {
       deny  all;
    }
    
}
```

### test environment
To clone production DB to test env, run [pipeline](https://gitlab.com/SlavikCA/donors-crm/-/pipelines/new?var[CLONE]=true)
with CLONE variable set to TRUE on `master` branch.

### License
This is copyrighted code.  
Contact slavikca@gmail.com for licensing
