<?php

use App\Http\Controllers\DashboardController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Route::redirect('/', 'login');

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');
Route::redirect('/home', 'dashboard/donations');

Route::get('/logout', function () {
    Auth::logout();

    return redirect('/');
})->name('logout');

Route::prefix('dashboard')->middleware(['auth'])->group(function () {
    Route::view('/', 'dashboard', ['page' => 'Dashboard']);
    Route::get('/donors', DashboardController::class.'@getDonors')->name('donors');
    //Route::get('/edit/{id}', DashboardController::class . '@editDonor');
    Route::post('/store-donor', DashboardController::class.'@storeDonor');
    Route::view('/add-donor', 'add-donor', ['page' => 'Add Donor']);
    Route::get('/delete/{id}', DashboardController::class.'@deleteDonor');
    Route::get('/donations', DashboardController::class.'@getDonations')->name('donations');
    Route::view('/add-donation', 'add-donation', ['page' => 'Add Donation']);
    Route::post('/store-donation', DashboardController::class.'@storeDonation');
    Route::get('/donor/{id}', DashboardController::class.'@getDonor');
    Route::get('/get-pdf', DashboardController::class.'@getPdf');
    Route::get('/users', DashboardController::class.'@getUsers')->name('users');
    Route::get('/user-toggle-active/{id}', DashboardController::class.'@toggleUsersActive');
    Route::get('/donations-categories', DashboardController::class.'@getDonationsCategories')->name('donations-categories');
    Route::post('/add-donations-categories', DashboardController::class.'@addDonationsCategories');
    Route::get('/donations-categories-toggle-active/{id}', DashboardController::class.'@toggleDonationsCategoriesActive');
    Route::post('/autocomplete', DashboardController::class.'@autocomplete');
    Route::post('/edit-donations', DashboardController::class.'@editDonations');
    Route::get('/delete-donation/{id}', DashboardController::class.'@deleteDonation');
    Route::get('/export-donors', DashboardController::class.'@exportDonors');
    Route::get('/export-donations', DashboardController::class.'@exportDonations');
    Route::get('/categories-report', DashboardController::class.'@getCategoriesReport');
    Route::get('/tags', DashboardController::class.'@getTags');
    Route::post('/tag-edit', DashboardController::class.'@editTag');
    Route::post('/tag-add', DashboardController::class.'@addTag');
    Route::get('/tag-delete/{id}', DashboardController::class.'@deleteTag');
    Route::post('/add-comment', DashboardController::class.'@addComment');
    Route::post('/donor/get-comment', DashboardController::class.'@getComment');
    Route::post('/donor/edit-comment', DashboardController::class.'@editComment');
    Route::get('/delete-comment/{id}', DashboardController::class.'@deleteComment');
});
