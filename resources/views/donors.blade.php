@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-2">
            <div class="form-group">
                <select class="form-control" name="year">
                    @for($i=$donationRange[1];$i>=$donationRange[0];$i--)
                        @if($i == \Illuminate\Support\Facades\Request::query('year'))
                            <option selected>{{ $i }}</option>
                        @else
                            <option>{{ $i }}</option>
                        @endif
                    @endfor
                </select>
            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <select class="form-control select2" multiple="multiple" name="include" data-placeholder="Include"
                        style="width: 100%;">
                    @foreach($tags as $tag)
                        <option data-id="{{ $tag->id }}" value="{{ $tag->name }}">{{ $tag->name }}</option>
                    @endforeach
                </select>

            </div>
        </div>
        <div class="col-2">
            <div class="form-group">
                <select class="form-control select2" multiple="multiple" name="exclude" data-placeholder="Exclude"
                        style="width: 100%;">
                    @foreach($tags as $tag)
                        @if($tag->name == 'inactive')
                            <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                            @continue
                        @endif
                        <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-2">
            <button type="button" class="btn btn-success filter invisible">Filter</button>
        </div>
        <div class="col-4 text-right">
            <a class="btn btn-primary pdf-donors" href="{{ url('/dashboard/get-pdf') }}?exclude=inactive"
               title="PDF with Avery 5160 address labels" target="_blank"><i class="fas fa-file-pdf"></i> Address labels</a>
            <a class="btn btn-success excel-donors" href="{{ url('/dashboard/export-donors') }}?exclude=inactive"
               title="Excel addresses" target="_blank"><i class="fas fa-file-excel"></i> Export
                Donors</a>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-12">
            <!-- STACKED BAR CHART -->
            <div class="card card-success">
                <!--
              <div class="card-header">
                <h3 class="card-title">Stacked Bar Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
                -->
                <div class="card-body">
                    <div class="chart">
                        <canvas id="stackedBarChart" style="height:230px; min-height:230px"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-12">
            <div class="card">

                <div class="card-body">
                    <div class="donors-form mb-2">
                        <form method="post" action="{{ url('dashboard/store-donor')}}" autocomplete="off">
                            {{ csrf_field() }}
                            <input type="hidden" name="action" value="add">
                            <div class="row">
                                <div class="col col-lg-4">
                                    <label>Name</label>
                                    <input type="text" class="form-control add-donor-field" name="name"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-4">
                                    <label>Email</label>
                                    <input type="email" class="form-control add-donor-field" name="email"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-4">
                                    <label>Phone</label>
                                    <input type="text" class="form-control add-donor-field" name="phone"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-3">
                                    <label>Street</label>
                                    <input type="text" class="form-control add-donor-field" name="street"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-3">
                                    <label>City</label>
                                    <input type="text" class="form-control add-donor-field" name="city"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-1">
                                    <label>State</label>
                                    <input type="text" class="form-control add-donor-field" name="state"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-1">
                                    <label>Zip</label>
                                    <input type="text" class="form-control add-donor-field" name="zip"
                                           autocomplete="disabled" onkeydown="if(event.keyCode===13){return false;}">
                                </div>
                                <div class="col col-lg-3">
                                    <label>Tags</label>
                                    <select class="form-control select2 add-donor-field" multiple="multiple"
                                            name="tags[]" style="width: 100%;">
                                        @foreach($tags as $tag)
                                            <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col col-lg-1">
                                    <button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <hr>
                    <table class="table table-bordered display datatable wrap">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>State</th>
                            <th>City</th>
                            <th>Street</th>
                            <th>Zip</th>
                            <th>Tags</th>
                            <th>Update</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr><td colspan="9"><p class="text-center">Loading...</p></td></tr>
                        {{--                        @foreach($donors as $donor)--}}
                        {{--                            <tr class="donor">--}}
                        {{--                                <td><a href="{{ url('dashboard/donor')}}/{{ $donor['id'] }}">{{ $donor['name'] }}</a>--}}
                        {{--                                </td>--}}
                        {{--                                <td>{{ $donor['email'] }}</td>--}}
                        {{--                                <td>{{ $donor['phone'] }}</td>--}}
                        {{--                                <td>{{ $donor['state'] }}</td>--}}
                        {{--                                <td>{{ $donor['city'] }}</td>--}}
                        {{--                                <td>{{ $donor['street'] }}</td>--}}
                        {{--                                <td>{{ $donor['zip'] }}</td>--}}
                        {{--                                <td>@foreach($donor->tags as $tag)--}}
                        {{--                                        <div--}}
                        {{--                                            style="background-color:#007bff;border-color:#006fe6;color:#fff;border-radius:4px;margin-bottom:5px;padding:0 5px;display:inline-block;">{{ $tag->name }}</div>@endforeach--}}
                        {{--                                </td>--}}
                        {{--                                <td data-order="{{$donor->updated_at->timestamp}}">{{ $donor->updated_at->timezone(Config::get('TIMEZONE'))->format('M j, Y @ g:ia') }}</td>--}}
                        {{--                            </tr>--}}
                        {{--                        @endforeach--}}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
    <script>
        function initDonationChart() {
            // Chart
            var areaChartData = {
                labels: {!! json_encode($months) !!},
                datasets: [
                    {
                        label: 'Mail',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: {!! json_encode($chart['mail']) !!}
                    },
                    {
                        label: 'PayPal',
                        backgroundColor: 'rgba(210, 214, 222, 1)',
                        borderColor: 'rgba(210, 214, 222, 1)',
                        pointRadius: false,
                        pointColor: 'rgba(210, 214, 222, 1)',
                        pointStrokeColor: '#c1c7d1',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(220,220,220,1)',
                        data: {!! json_encode($chart['paypal']) !!}
                    },
                    {
                        label: 'Personal',
                        backgroundColor: 'rgba(310, 214, 222, 1)',
                        borderColor: 'rgba(310, 214, 222, 1)',
                        pointRadius: false,
                        pointColor: 'rgba(310, 214, 222, 1)',
                        pointStrokeColor: '#c1c7d8',
                        pointHighlightFill: '#ffa',
                        pointHighlightStroke: 'rgba(320,220,220,1)',
                        data: {!! json_encode($chart['personal']) !!}
                    },
                ]
            }

            var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
            var stackedBarChartData = jQuery.extend(true, {}, areaChartData)

            var stackedBarChartOptions = {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        stacked: true,
                    }],
                    yAxes: [{
                        stacked: true
                    }]
                }
            }

            var stackedBarChart = new Chart(stackedBarChartCanvas, {
                type: 'bar',
                data: stackedBarChartData,
                options: stackedBarChartOptions
            })
        }

        function initDonationsTable() {
            let $include = $('.select2[name=include]'),
                $exclude = $('.select2[name=exclude]'),
                $exportPdf = $('.pdf-donors'),
                $exportReport = $('.excel-donors'),
                $donorCount = $('.amount-donors');

            let $table = $('.datatable');
            let tableData = null;

            $.get('/api/donors', function (data) {
                tableData = data;
                $table = $table.DataTable({
                    data: data,
                    columns: [
                        {
                            data: 'name', render: function (data, type, row) {
                                if (type === 'display') {
                                    return '<a href="/dashboard/donor/' + row.id + '">' + row.name + '</a>';
                                }
                                return data;
                            }
                        },
                        {data: 'email', orderable: false},
                        {data: 'phone', orderable: false},
                        {data: 'state'},
                        {data: 'city'},
                        {data: 'street', orderable: false},
                        {data: 'zip'},
                        {
                            data: 'tags', orderable: false, render: function (data, type, row) {
                                return row.tags.map(function (item) {
                                    if (type === 'display') {
                                        return '<div style="background-color:#007bff;border-color:#006fe6;color:#fff;border-radius:4px;margin-bottom:5px;padding:0 5px;display:inline-block;">' + item.name + '</div>';
                                    }
                                    return data;
                                }).join('');
                            }
                        },
                        {
                            data: 'updated_at', render: function (data, type, row) {
                                // If display or filter data is requested, format the date
                                if (type === 'display') {
                                    return formatDate(new Date(row.updated_at));
                                }
                                return data;
                            }
                        },
                    ],
                    searchable: false,
                    paging: false,
                    orderClasses: false,
                });

                function formatDate(d) {
                    const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
                    const pm = d.getHours() > 12;
                    return [
                        months[d.getMonth()],
                        d.getDate() + ',',
                        (d.getFullYear() + '').substr(-2),
                        ].join(' ') + [
                        ' @ ',
                        (d.getHours() % 12) + 1,
                        ':',
                        d.getMinutes(),
                        pm ? 'pm' : 'am'
                    ].join('');
                }
                applyTableFilters(); // initial load
            }, 'json');

            $('.add-donor-field').on('change keyup focusin', function () {
                let $that = $(this),
                    ix = getFilterableColumnIx($that);
                if (typeof ix !== 'undefined') {
                    $table.search('').column(ix).search($that.val()).draw();
                }
            });

            function getFilterableColumnIx($item) {
                const colIxs = {
                    name: 0,
                    email: 1,
                    phone: 2,
                    state: 3,
                    city: 4,
                    street: 5,
                    zip: 6,
                };

                return colIxs[$item.attr('name')];
            }

            $('.select2[name=include],.select2[name=exclude]').select2({
                theme: 'bootstrap4'
            }).on('change', function (e) {
                applyTableFilters();
                updateExportLinks();
            });

            function updateExportLinks() {
                let includeItems = $include.val(),
                    excludeItems = $exclude.val();

                let params = [];
                if (includeItems.length > 0) {
                    params.push("include=" + includeItems.join(','));
                }
                if (excludeItems.length > 0) {
                    params.push("exclude=" + excludeItems.join(','));
                }

                if (params.length > 0) {
                    $exportPdf.attr('href', 'get-pdf?' + params.join('&'))
                    $exportReport.attr('href', 'export-donors?' + params.join('&'))
                } else {
                    $exportPdf.attr('href', 'get-pdf')
                    $exportReport.attr('href', 'export-donors')
                }
            }

            function applyTableFilters() {
                let includeItems = $include.val(),
                    excludeItems = $exclude.val();

                if (!includeItems.length && !excludeItems.length) {
                    setTableData();
                    return;
                }

                var filtered = tableData.filter(function (value) {
                    const tags = value.tags.map(x => x.name).join(', ');
                    if (includeItems.length) {
                        let isFound = false;
                        for (let item of includeItems) {
                            if (tags.indexOf(item) > -1) {
                                isFound = true;
                                break;
                            }
                        }
                        if (!isFound) {
                            return false;
                        }
                    }

                    if (excludeItems.length) {
                        for (let item of excludeItems) {
                            if (tags.indexOf(item) > -1) return false;
                        }
                    }

                    return true;
                });

                setTableData(filtered);
            }

            function setTableData(data) {
                if (typeof data === 'undefined') {
                    data = tableData; // use initial table data
                }
                $table.clear();
                $table.rows.add(data);
                $table.draw();

                $donorCount.text(data.length)
            }

            // указание тэгов если есть параметр include

            const link = location.href.split('?');
            if (link.length > 1) {
                const filters = link[1].split('&');
                for (const filter of filters) {
                    const parts = filter.split('=');
                    if (parts.length <= 1) {
                        continue;
                    }
                    const value = decodeURI(parts[1]);
                    if (parts[0] === 'include') {
                        $include.val(value).trigger('change');
                    } else if (parts[1] === 'exclude') {
                        $exclude.val(value).trigger('change');
                    }
                }

            }
        }

        $(document).ready(function () {
            initDonationChart();
            initDonationsTable();
        })
    </script>
@endsection
