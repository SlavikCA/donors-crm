<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <style>
	body {
		/*background-color: #ccc;*/
		margin:20px 0px -30px 0px;
		font-size: 14px;
	}
	.donors {
		float:left;
		width:33.33%;
		text-align:center;
		margin:auto 0;
		padding:0px;
	}
	.row {
		clear:both;
		height:9.4%;
		margin:0 -50px;
	}
	.page-break {
		page-break-after: always;
	}
  </style>
</head>
<body>
		@foreach($lists as $list)
			<?php //dd($list); ?>
			<div class="list">
				@foreach($list as $row)
				<?php //dd($row); ?>
					<div class="row">
						@foreach($row as $donor)
						<?php //$donor = (array)$donor; ?>
							<div class="donors" style="margin-bottom:3px;">
                                RETURN SERVICE REQUESTED
								<div>{{ $donor['name'] }}</div>
								<div style="margin-top:4px;">{{ $donor['street'] }}</div>
								<div style="margin-top:4px;">{{ $donor['city'] }}, {{ $donor['state'] }} {{ $donor['zip'] }}</div>
							</div>
						@endforeach
					</div>
				@endforeach

			</div>
			<div class="page-break"></div>
		@endforeach

</body>
</html>
