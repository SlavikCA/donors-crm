@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-6">
			<form id="add-donation" method="post" action="{{ url('dashboard/store-donation')}}">
				{{ csrf_field() }}
				<div class="form-group choose-donor-form">
					<label>Donor</label>
					<select class="form-control select2" style="width: 100%;" name="donor">
						@foreach(\App\Models\Donor::all() as $donor)
							<option value="{{ $donor->id }}">{{ $donor->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group add-donor-form d-none">
					<label for="name">Name</label>
					<input id="name" type="text" class="form-control" name="name">
				</div>
				<div class="form-group add-donor-form d-none">
					<label for="address">Address</label>
					<input id="address" type="text" class="form-control" name="address">
				</div>
				<div class="form-group add-donor-form d-none">
					<label for="church">Church</label>
					<input id="church" type="text" class="form-control" name="church">
				</div>
				<div class="form-group add-donor-form d-none">
					<label for="level">Level</label>
					<select class="form-control" name="level" id="level">
						<option value="1">Level 1</option>
						<option value="2">Level 2</option>
						<option value="3">Level 3</option>
						<option value="4">Level 4</option>
					</select>
				</div>
				<div class="form-group">
					<label for="date">Date</label>
					<input id="date" type="date" class="form-control" name="date">
				</div>
				<div class="form-group">
					<label for="amount">Amount</label>
					<input id="amount" type="number" class="form-control" name="amount">
				</div>
				<div class="form-group">
					<label for="categoryId">Category</label>
					<select id="categoryId" name="categoryId" class="form-control">
						@foreach(\App\Models\DonationsCategories::where('active', '=', 1)->get() as $category)
							<option value="{{ $category->id }}">{{ $category->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="form-group">
					<label for="type">Type</label>
					<select id="type" name="type" class="form-control">
						<option value="paypal">PayPal</option>
						<option value="mail">Mail</option>
						<option value="personal">Personal</option>
					</select>
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
				</div>
			</form>
		</div>
		<div class="col-6">
			<form>
				<div class="form-group">
					<label style="color:rgb(244, 246, 249);">a</label>
					<div>
						<button type="button" class="btn btn-success form-control col-3 add-donor"><i class="fa fa-plus"></i> Add donor</button>
						<button type="button" class="btn btn-success form-control col-4 choose-donor d-none"><i class="fas fa-sync-alt"></i> Choose donor</button>
					</div>
				</div>
			</form>
		</div>
	</div>
@endsection
