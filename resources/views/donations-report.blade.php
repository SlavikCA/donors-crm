@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-6">
			<a href="{{ route('donations') }}"><i class="fas fa-arrow-left"></i> Back</a>
		</div>
	</div>
	<p></p>
	@foreach($report as $period => $donations)
		@if($donations == 0)
			@continue
		@endif
		<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-header">
					<h3>{{ $period }}</h3>
				</div>
				<div class="card-body">
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td>Donations</td>
								<td>{{ $donations }}</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<p></p>
	@endforeach
@endsection