@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th><a href="{{ url('dashboard/tags') }}?sort=name">Tag Name</a></th>
								<th>Description</th>
								<th><a href="{{ url('dashboard/tags') }}?sort=amount">Amount Users</a></th>
								<th>Actions</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td><input type="text" class="form-control" name="newname" form="addtag" required></td>
								<td><input type="text" class="form-control" name="newdescription" form="addtag" required></td>
								<td></td>
								<td>
									<form method="POST" action="{{ url('/dashboard/tag-add') }}" id="addtag">
										{{ csrf_field() }}
										<button type="submit" class="btn btn-success tag-add"><i class="fa fa-plus"></i> Add</button>
									</form>
								</td>
							</tr>
							@foreach($tags as $tag)
								<tr data-id="{{ $tag->id }}">
									<td class="tag-name">
                                        <a href="{{ url('/dashboard/donors') }}?include={{ $tag->name }}">{{ $tag->name }}</a>
                                        @if($tag->is_system)
                                        <span class="ml-2 badge badge-pill badge-info">system</span>
                                        @endif
                                    </td>
									<td class="tag-description">{{ $tag->description }}</td>
									<td>{{ $tag->amount }}</td>
									<td class="tag-actions">
                                        @if ($tag->is_system)
                                            @continue(1);
                                        @endif
										<a href="#" data-action="yes" data-id="{{ $tag->id }}" data-name="{{ $tag->name }}" data-description="{{ $tag->description }}" class="tag-edit"><i class="fa fa-edit"></i></a>
										<a href="#" class="tag-delete" data-toggle="modal" data-target="#modal-sm-{{ $tag->id }}"><i class="fa fa-trash"></i></a>
										<div class="modal fade" id="modal-sm-{{ $tag->id }}">
											<div class="modal-dialog modal-sm">
											  <div class="modal-content">
												<div class="modal-header">
												  <!-- <h4 class="modal-title">Small Modal</h4> -->
												  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
													<span aria-hidden="true">&times;</span>
												  </button>
												</div>
												<div class="modal-body">
												  <p>Are you sure to delete tag {{ $tag->name }}?</p>
												</div>
												<div class="modal-footer justify-content-between">
												  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												  <a role="button" class="btn btn-primary" href="{{ url('dashboard/tag-delete/') }}/{{ $tag->id }}">Delete</a>
												</div>
											  </div>
											  <!-- /.modal-content -->
											</div>
											<!-- /.modal-dialog -->
										  </div>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
