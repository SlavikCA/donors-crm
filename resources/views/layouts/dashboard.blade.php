<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Donations</title>
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/fontawesome-free/css/all.min.css') }}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('adminlte/dist/css/adminlte.min.css') }}">
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.css') }}">
  <!-- Datatables picker -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">

  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('adminlte/plugins/summernote/summernote-bs4.css') }}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
  .string:hover {
	background-color: #ccc;
	cursor: pointer;
  }
  </style>
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ url('/dashboard/donations') }}" class="brand-link">
      <img src="" alt="Donations Crm Logo" class="d-none brand-image img-circle elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">Donations</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="" class="img-circle elevation-2 d-none" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item d-none">
            <a href="{{ url('/dashboard') }}" class="nav-link @if($page == 'Dashboard')active @endif">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ url('/dashboard/donors') }}" class="nav-link @if($page == 'Donors')active @endif">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Donors
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ url('/dashboard/donations') }}" class="nav-link @if($page == 'Donations')active @endif">
              <i class="nav-icon fas fa-dollar-sign"></i>
              <p>
                Donations
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ url('/dashboard/donations-categories') }}" class="nav-link @if($page == 'Donations Categories')active @endif">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Donations Categories
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ url('/dashboard/categories-report') }}" class="nav-link @if($page == 'Categories Report')active @endif">
              <i class="nav-icon fas fa-list"></i>
              <p>
                Categories Report
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ url('/dashboard/tags') }}" class="nav-link @if($page == 'Tags')active @endif">
              <i class="nav-icon fas fa-tags"></i>
              <p>
                Tags
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ url('/dashboard/users') }}" class="nav-link @if($page == 'Users')active @endif">
              <i class="nav-icon fas fa-users"></i>
              <p>
                Users
              </p>
            </a>
          </li>
		  <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link">
              <i class="nav-icon fas fa-sign-out-alt"></i>
              <p>
                Logout
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">{{ $page }}@if($page == 'Donors'): <span class="amount-donors">{{ $countDonors }}</span> @endif</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">

          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @section('content')
        @show
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">

  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<div class="default-modals">
    <div class="modal fade" id="error-modal">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">An error has occurred</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p class="text-danger"></p>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery -->
<script src="{{ asset('adminlte/plugins/jquery/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<!-- Select2 -->
<script src="{{ asset('adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
<!-- ChartJS -->
<script src="{{ asset('adminlte/plugins/chart.js/Chart.min.js') }}"></script>
<!-- Sparkline
<script src="{{ asset('adminlte/plugins/sparklines/sparkline.js') }}"></script>
 -->
<!-- JQVMap
<script src="{{ asset('adminlte/plugins/jqvmap/jquery.vmap.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js') }}"></script>
 -->
<!-- jQuery Knob Chart -->
<script src="{{ asset('adminlte/plugins/jquery-knob/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('adminlte/plugins/moment/moment.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/daterangepicker/daterangepicker.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('adminlte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
<script src="{{ asset('adminlte/plugins/datatables-select/js/dataTables.select.js') }}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ asset('adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
<!-- Summernote -->
<script src="{{ asset('adminlte/plugins/summernote/summernote-bs4.min.js') }}"></script>
<!-- overlayScrollbars -->
<script src="{{ asset('adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('adminlte/dist/js/adminlte.js') }}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes)
<script src="{{ asset('adminlte/dist/js/pages/dashboard.js') }}"></script>
 -->
<script>
    $(function () {
        // Initialize Select2 Elements
        $('.select2bs4').select2({
            theme: 'bootstrap4'
        })

        // Initialize Select2 Elements
        $('.select2').select2({
            theme: 'bootstrap4'
        })
    })

    function showErrorModal(error, title) {
        var defaultTitle = 'An error has occurred';
        $modal = $('#error-modal');
        $modal.find('.modal-title').text(title || defaultTitle);
        $modal.find('.modal-body p').text(error);
        $modal.modal('show');
        $modal.on('hidden.bs.modal', function (e) {
            // clear contents
            $modal.find('.modal-title').text(defaultTitle);
            $modal.find('.modal-body p').text('');
        })
    }

	$(document).ready(function(){
		$.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			}
		});

		$('body').on('click', '.add-donor', function(e){
			$('.add-donor-form').removeClass('d-none');
			$(this).addClass('d-none');
			$('.choose-donor-form').addClass('d-none');
			$('.choose-donor').removeClass('d-none');
			$('#add-donation').append('<input type="hidden" name="action" value="add-donor">');
		});

		$('body').on('click', '.choose-donor', function(e){
			$('.add-donor-form').addClass('d-none');
			$(this).addClass('d-none');
			$('.choose-donor-form').removeClass('d-none');
			$('.add-donor').removeClass('d-none');
			$('#add-donation input[name="action"]').remove();
		});

		//автозаполнение формы добавления пользователей
		// $('body').on('keyup', '.add-donor-field', function(e){
		// 	string = $(this).val();
		// 	string = string.toLowerCase();
		// 	name = $(this).attr('name');
		// 	$('.donor').addClass('d-none');
		// 	$('tr[data-'+name+'*="'+string+'"]').removeClass('d-none');
		// 	if(string == ''){
		// 		$('tr').removeClass('d-none');
		// 	}
		// });

		//кнопка "добавить" в пожертвованиях отключена, если не введена хотя бы одна сумма
		$('body').on('change', 'input[name^="categoryId"]', function(e){
			arr = [];
			$('input[name^="categoryId"]').each(function(element){
				arr.push($(this).val());
			});
			var show = false;
			arr.forEach(function(element){
				if(element > 0){
					return show = true;
				}
			});
			if(show){
				$('.add-donations').prop('disabled', false);
			}
			else {
				$('.add-donations').prop('disabled', true);
			}
		});
	});
</script>
@if($page == 'Categories Report')
<script>
    (function () {
        // Chart
        var areaChartData = {
            labels : {!! json_encode($months) !!},
            datasets: [
                <?php
                foreach($categories as $key => $category){
                    echo '{';
                    echo 'label:\''.$category->name.'\',';
                    echo 'backgroundColor:\''.$colors[$key].'\',';
                    echo 'pointRadius:false,';
                    echo 'data:[';
                    foreach($chart[$category->id] as $value){
                        echo $value.',';
                    }
                    echo ']';
                    echo '},';
                }
                ?>
            ]
        }

        var barChartData = jQuery.extend(true, {}, areaChartData)

        var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
        var stackedBarChartData = jQuery.extend(true, {}, barChartData)

        var stackedBarChartOptions = {
            responsive              : true,
            maintainAspectRatio     : false,
            scales: {
                xAxes: [{
                    stacked: true,
                }],
                yAxes: [{
                    stacked: true
                }]
            }
        }

        var stackedBarChart = new Chart(stackedBarChartCanvas, {
            type: 'bar',
            data: stackedBarChartData,
            options: stackedBarChartOptions
        })
        //===============

        /*
        $('body').on('change', 'select[name="year"]', function(e){
            year = $(this).val();
            href = window.location.href.split('?');
            location.href = href[0]+'?year='+year+'';
        });
        */
    })();
</script>
@endif
@if($page == 'Tags')
<script>
    (function () {

        $('body').on('click', '.tag-edit', function (e) {
            e.preventDefault();
            if ($(this).attr('data-action') == 'no') {
                return;
            }
            name = $(this).attr('data-name');
            description = $(this).attr('data-description');
            id = $(this).attr('data-id');
            $(this).parents('td').siblings('td.tag-name').html('<input type="text" class="form-control" name="name" value="' + name + '">');
            $(this).parents('td').siblings('td.tag-description').html('<input type="text" class="form-control" name="description" value="' + description + '">');
            $(this).parents('td').html('<a href="#" data-id="' + id + '" class="submit-edit"><i class="fas fa-check"></i></a> <a href="#" class="close-edit" data-id="' + id + '" data-name="' + name + '" data-description="' + description + '"><i class="fas fa-times"></i></a>');
            $('.tag-edit, .tag-delete').css({'cursor': 'not-allowed'}).attr('data-action', 'no');
        });

        $('body').on('click', '.submit-edit', function (e) {
            e.preventDefault();
            name = $('input[name="name"]').val();
            description = $('input[name="description"]').val();
            id = $(this).attr('data-id');
            $.post('tag-edit', {'id': id, 'name': name, 'description': description}, function (data) {
                $('tr[data-id="' + id + '"] td.tag-name').html('<a href="/dashboard/donors?include=' + data.name + '" >' + data.name + '</a>');
                $('tr[data-id="' + id + '"] td.tag-description').html(data.description);
                $('tr[data-id="' + id + '"] td.tag-actions').html('<a href="#" data-action="yes" data-id="' + id + '" data-name="' + name + '" data-description="' + description + '" class="tag-edit"><i class="fa fa-edit"></i></a><a href="#" class="tag-delete"><i class="fa fa-trash"></i></a>');
            }, 'json').fail(function (error) {
                if (!error.responseJSON) {
                    console.log(error);
                    return showErrorModal('An unknown error has occurred. Please try again later.');
                }
                if (error.responseJSON.errors) {
                    showErrorModal($.map(error.responseJSON.errors, function (e) {
                        return e;
                    }), 'Validation Error');
                } else {
                    showErrorModal(error.responseJSON);
                }
            });
        });

        $('body').on('click', '.close-edit', function (e) {
            e.preventDefault();
            name = $(this).attr('data-name');
            description = $(this).attr('data-description');
            id = $(this).attr('data-id');
            $(this).parents('td').siblings('td.tag-name').html(name);
            $(this).parents('td').siblings('td.tag-description').html(description);
            $(this).parents('td').html('<a href="#" data-action="yes" data-id="' + id + '" data-name="' + name + '" data-description="' + description + '" class="tag-edit"><i class="fa fa-edit"></i></a><a href="#" class="tag-delete"><i class="fa fa-trash"></i></a>');
            $('.tag-edit, .tag-delete').css({'cursor': 'pointer'}).attr('data-action', 'yes');
        });
    })();
</script>
@endif
<script>
	$('body').on('change', 'select[name="year"]', function(e){
		year = $(this).val();
		href = window.location.href.split('?');
		location.href = href[0]+'?year='+year+'';
	});
</script>

@yield('page_script')
</body>
</html>
