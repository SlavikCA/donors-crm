@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-2">
			<div class="form-group">
				<select class="form-control" name="year">
					<!-- <option disabled selected>Choose year</option> -->
					{{-- @for($i=$yearBegin;$i<=$yearEnd;$i++) --}}
					@for($i=$yearEnd;$i>=$yearBegin;$i--)
						
						@if($i == \Illuminate\Support\Facades\Request::query('year'))
							<option selected>{{ $i }}</option>
							@continue
						@endif

						<option>{{ $i }}</option>
					@endfor
				</select>
			</div>
		</div>
	</div>
	
	<div class="row">
		<div class="col-12">
			<!-- STACKED BAR CHART -->
            <div class="card card-success">
              <div class="card-body">
                <div class="chart">
                  <canvas id="stackedBarChart" style="height:600px; min-height:200px"></canvas>
                </div>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
		</div>
	</div>
	
	<div class="row">
		<div class="col-12">
		<!--
			<div class="card">
				<div class="card-body">
				-->
					<table class="table table-bordered">
						<thead>
							<tr>
								<th></th>
								@foreach($tableMonth as $month)
								<th>{{ $month }}</th>
								@endforeach
								<th style="background-color:#ccc;">Total</th>
							</tr>
						</thead>
						<tbody>
							@foreach($categories as $key => $category)
								<tr>
									<td style="background-color:{{ $colors[$key] }};font-weight:bold;">{{ $category->name }}</td>
									<?php $total=0; ?>
									@foreach($chart[$category->id] as $value)
										<?php $total += $value; ?>
										<td>{{ $value }}</td>
									@endforeach
									<td class="font-weight-bold" style="background-color:#ccc;">{{ $total }}</td>
								</tr>
							@endforeach
							<tr>
								<td class="font-weight-bold" style="background-color:#ccc;">Total</td>
								<?php $total = 0; ?>
								@foreach($sumDonations as $sum)
									<?php $total += $sum; ?>
									<td class="font-weight-bold" style="background-color:#ccc;">{{ $sum }}</td>
								@endforeach
								<td class="font-weight-bold" style="background-color:#ccc;">{{ $total }}</td>
							</tr>
						</tbody>
					</table>
			<!--
				</div>
			</div>
			-->
		</div>
	</div>
@endsection