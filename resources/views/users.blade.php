@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($users as $user)
                            <tr>
                                <td>{{ $user->id }}</td>
                                <td>
                                    <span @if($user->active == 0) class="font-italic text-muted" @endif>{{ $user->name }}</span>
                                </td>
                                <td>
                                    <span @if($user->active == 0) class="font-italic text-muted" @endif>{{ $user->email }}</span>
                                </td>
                                <td>
                                    <a href="user-toggle-active/{{ $user->id }}?active={{ $user->active ? 'false' : 'true' }}">
                                        <i class="fa fa-power-off @if($user->active == 0) text-danger @else text-success @endif"></i>
                                    </a>
                                </td>
                            <tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
