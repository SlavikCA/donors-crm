@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-6">
			<form method="post" action="{{ url('dashboard/store-donor')}}">
				{{ csrf_field() }}
				<input type="hidden" name="action" value="edit">
				<input type="hidden" name="id" value="{{ $donor->id }}">
				<div class="form-group">
					<label for="name">Name</label>
					<input id="name" type="text" class="form-control" name="name" value="{{ $donor->name }}">
				</div>
				<div class="form-group">
					<label for="street">Street</label>
					<input id="street" type="text" class="form-control" name="street" value="{{ $donor->street }}">
				</div>
				<div class="form-group">
					<label for="city">City</label>
					<input id="city" type="text" class="form-control" name="city" value="{{ $donor->city }}">
				</div>
				<div class="form-group">
					<label for="state">State</label>
					<input id="state" type="text" class="form-control" name="state" value="{{ $donor->state }}">
				</div>
				<div class="form-group">
					<label for="zip">Zip Code</label>
					<input id="zip" type="text" class="form-control" name="zip" value="{{ $donor->zip }}">
				</div>
				<div class="form-group">
					<label for="church">Church</label>
					<input id="church" type="text" class="form-control" name="church" value="{{ $donor->church }}">
				</div>
				<div class="form-group">
					<label for="level">Level</label>
					<select class="form-control" name="level" id="level">
						<option value="1" @if($donor->donors_level == 1) selected @endif>Level 1</option>
						<option value="2" @if($donor->donors_level == 2) selected @endif>Level 2</option>
						<option value="3" @if($donor->donors_level == 3) selected @endif>Level 3</option>
						<option value="4" @if($donor->donors_level == 4) selected @endif>Level 4</option>
					</select>	
				</div>
				<div class="form-group">
					<input type="submit" class="btn btn-success" value="Submit">
				</div>
			</form>
		</div>
	</div>
@endsection