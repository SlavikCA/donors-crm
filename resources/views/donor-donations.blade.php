@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-6">
            <a href="{{ route('donors') }}"><i class="fas fa-arrow-left"></i> Back</a>
        </div>
    </div>
    <div class="row">
        <div class="col-6">
            <form method="post" action="{{ url('dashboard/store-donor')}}">
                {{ csrf_field() }}
                <input type="hidden" name="action" value="edit">
                <input type="hidden" name="id" value="{{ $donor->id }}">
                <input type="hidden" name="editfields">
                <!-- <input type="hidden" name="oldvalue">
                <input type="hidden" name="newvalue"> -->
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" class="form-control" name="name" value="{{ $donor->name }}">
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" class="form-control" name="email" value="{{ $donor->email }}">
                </div>
                <div class="form-group">
                    <label for="phone">Phone</label>
                    <input id="phone" type="text" class="form-control" name="phone" value="{{ $donor->phone }}">
                </div>
                <div class="form-group">
                    <label for="street">Street</label>
                    <input id="street" type="text" class="form-control" name="street" value="{{ $donor->street }}">
                </div>
                <div class="form-group">
                    <label for="city">City</label>
                    <input id="city" type="text" class="form-control" name="city" value="{{ $donor->city }}">
                </div>
                <div class="form-group">
                    <label for="state">State</label>
                    <input id="state" type="text" class="form-control" name="state" value="{{ $donor->state }}">
                </div>
                <div class="form-group">
                    <label for="zip">Zip Code</label>
                    <input id="zip" type="text" class="form-control" name="zip" value="{{ $donor->zip }}">
                </div>
                <div class="form-group">
                    <label for="tags">Tags</label>
                    <select id="tags" class="form-control select2" multiple="multiple" name="tags[]"
                            style="width: 100%;">
                        @foreach(\App\Models\Tag::all() as $tag)
                            @if(in_array($tag->name, $donor_tag_names))
                                <option value="{{ $tag->name }}" selected>{{ $tag->name }}</option>
                            @else
                                <option value="{{ $tag->name }}">{{ $tag->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <a href="{{ url('dashboard/delete') }}/{{ $donor->id }}" class="btn btn-danger"
                       role="button">Delete</a>
                    <input type="submit" class="btn btn-success" value="Save">
                </div>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <h3>Activity</h3>
            <form method="POST" action="{{ url('dashboard/add-comment') }}">
                {{ csrf_field() }}
                <input type="hidden" name="donor_id" value="{{ $donor->id }}">
                <div class="form-group">
                    <textarea class="col-12" name="comment"></textarea>
                </div>
                <div>
                    <input type="submit" class="btn btn-success" value="Add comment">
                </div>
            </form>
            <p></p>
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered activity-log">
                        <thead>
                        <tr>
                            <th>Date</th>
                            <th>Type</th>
                            <th>Amount</th>
                            <th>Categories</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($activity as $item)
                            @if($item['type'] === 'DONATION')
                                <tr class="donation-log" data-id="{{ $item['data']->id }}">
                                    <td data-field="date">{{ $item['date'] }}</td>
                                    <td data-field="type">{{ $item['data']->type }}</td>
                                    <td data-field="amount">{{ $item['data']->amount }}</td>
                                    <td data-field="category">
                                        {{ $item['data']->category->name }}
                                        <a href="#"
                                           class="float-right action-donations-{{ $item['data']->id }} edit-donations invisible"
                                           data-id="{{ $item['data']->id }}" data-donor-id="{{ $donor->id }}"><i
                                                class="fa fa-edit"></i></a>
                                        <a href="{{ url('/dashboard/delete-donation') }}/{{ $item['data']->id }}"
                                           class="float-right mr-1 action-donations-{{ $item['data']->id }} invisible"><i
                                                class="fa fa-trash"></i></a>
                                    </td>
                                <tr>
                            @elseif($item['type'] === 'COMMENT')
                                <tr class="comment-log" data-id="{{ $item['data']->id }}">
                                    <td data-field="date">{{ $item['date'] }}</td>
                                    <td data-field="comment" colspan="3">
                                        {!! str_replace("\n", "<br>", $item['data']->comment) !!}
                                        <a href="#"
                                           class="float-right action-comments-{{ $item['data']->id }} edit-comments invisible"
                                           data-id="{{ $item['data']->id }}"><i class="fa fa-edit"></i></a>
                                        <a href="{{ url('/dashboard/delete-comment') }}/{{ $item['data']->id }}"
                                           class="float-right mr-1 action-comments-{{ $item['data']->id }} invisible"><i
                                                class="fa fa-trash"></i></a>
                                    </td>
                                <tr>
                            @endif
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
    <script>
        (function () {
            const $body = $('body');
            $body.on('change', '.form-control', function (e) {
                let name = $(this).attr('name'),
                    val = $('input[name="editfields"]').val() + ' ' + name;
                $('input[name="editfields"]').val(val);
            });

            // редактирование
            $('table.activity-log').on('mouseover', 'tr', function (e) {
                var $that = $(this),
                    id = $that.attr('data-id');
                $that.find('.action-donations-' + id).removeClass('invisible');
                $that.find('.action-comments-' + id).removeClass('invisible');
            }).on('mouseout', 'tr', function (e) {
                var $that = $(this),
                    id = $that.attr('data-id');
                $that.find('.action-donations-' + id).addClass('invisible');
                $that.find('.action-comments-' + id).addClass('invisible');
            });

            $body.on('click', 'a.edit-donations, a.edit-comments', function (e) {
                e.preventDefault();
                var $that = $(this),
                    id = $that.attr('data-id');
                var $editor = $('<tr data-id="' + id + '" data-type="edit"></tr>');
                $editor.append('<input type="hidden" name="donorEdit" value="' + $that.attr('data-donor-id') + '">');
                $that.parents('tr[data-id]').find('td').each(function () {
                    let $that = $(this),
                        field = $that.attr('data-field'),
                        text = $that.text().trim();

                    if (field === 'date') {
                        $editor.append('<td><input type="date" class="form-control" name="dateEdit" value="' + text + '"></td>');
                    } else if (field === 'category') {
                        $.get('/api/donation-categories', function (data) {
                            $editor.append('<td><select class="form-control" style="width: 100%;" name="categoryEdit"></select><p></p><a href="#" class="btn btn-success mr-1 edit-save" data-id="' + id + '">Save</a><a href="#" class="btn btn-danger edit-cancel" data-id="' + id + '">Cancel</a></td>');
                            data.forEach(function (el) {
                                if (text === el.name) {
                                    $editor.find('select[name="categoryEdit"]').append('<option selected>' + el.name + '</option>');
                                } else {
                                    $editor.find('select[name="categoryEdit"]').append('<option>' + el.name + '</option>');
                                }
                            });
                        }, 'json');
                    } else if (field === 'amount') {
                        $editor.append('<td><input class="form-control" type="number" name="amountEdit" value="' + text + '"></td>');
                    } else if (field === 'type') {
                        const types = ['mail', 'personal', 'paypal'];
                        $editor.append('<td><select class="form-control" style="width: 100%;" name="typeEdit"></select></td>');
                        types.forEach(function (type) {
                            if (text === type) {
                                $editor.find('select[name="typeEdit"]').append('<option selected>' + type + '</option>');
                            } else {
                                $editor.find('select[name="typeEdit"]').append('<option>' + type + '</option>');
                            }
                        });
                    } else if (field === 'comment') {
                        $.post('get-comment', {'id': id}, function (data) {
                            $editor.append('<td colspan="3"><textarea class="form-control" name="commentEdit">'
                                + data.comment.replaceAll('<br>', "\n")
                                + '</textarea><p></p><a href="#" class="btn btn-success mr-1 edit-save-comment" data-id="' + id + '">Save</a><a href="#" class="btn btn-danger edit-cancel" data-id="' + id + '">Cancel</a></td>');
                        }, 'json');
                    }
                });
                $that.parents('tr[data-type!="edit"]').before($editor).addClass('d-none');
            });

            $body.on('click', 'a.edit-cancel', function (e) {
                e.preventDefault();
                var $that = $(this);
                $that.parents('tr[data-id]').siblings('.d-none').removeClass('d-none');
                $that.parents('[data-type="edit"]').remove();
            });

            $body.on('click', 'a.edit-save', function (e) {
                e.preventDefault();
                var $that = $(this),
                    $parent = $that.parents('tr[data-id]'),
                    payload = {
                        id: $that.attr('data-id'),
                        donorId: $parent.find('input[name="donorEdit"]').val(),
                        amount: $parent.find('input[name="amountEdit"]').val(),
                        category: $parent.find('select[name="categoryEdit"]').val(),
                        type: $parent.find('select[name="typeEdit"]').val(),
                        date: $parent.find('input[name="dateEdit"]').val(),
                    };

                $.post('edit-donations', payload, function (data) {
                    if (data === 'edit-donations') {
                        location.reload();
                    }
                });
            });

            $body.on('click', 'a.edit-save-comment', function (e) {
                e.preventDefault();
                var $that = $(this),
                    $parent = $that.parents('tr[data-id]'),
                    payload = {
                        'id': $that.attr('data-id'),
                        'comment': $('textarea[name="commentEdit"]').val(),
                        'date': $('input[name="dateEdit"]').val()
                    };
                $.post('edit-comment', payload, function (data) {
                    const date = (new Date(data.created_at)).toISOString().split('T')[0];
                    const $sibling = $parent.siblings('.d-none');
                    $sibling.find('td[data-field="date"]').html(date);
                    $sibling.find('td[data-field="comment"]').html(data.comment.replaceAll("\n", "<br>") + '<a href="#" class="float-right action-comments-' + data.id + ' edit-comments invisible" data-id="' + data.id + '"><i class="fa fa-edit"></i></a><a href="delete-comment/' + data.id + '" class="float-right mr-1 action-comments-' + data.id + ' invisible"><i class="fa fa-trash"></i></a>');
                    $sibling.removeClass('d-none');
                    $that.parents('[data-type="edit"]').remove();
                }, 'json');
            });
        })();
    </script>
@endsection
