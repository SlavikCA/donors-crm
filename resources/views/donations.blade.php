@extends('layouts.dashboard')

@section('content')
    <div class="row">
        <div class="col-2">
            <div class="form-group">
                <select class="form-control" name="year">
                    @for($i=$donationRange[1];$i>=$donationRange[0];$i--)
                        @if($i == \Illuminate\Support\Facades\Request::query('year'))
                            <option selected>{{ $i }}</option>
                        @else
                            <option>{{ $i }}</option>
                        @endif
                    @endfor
                </select>
            </div>
        </div>
        <div class="offset-7 col-3 text-right">
            <a class="btn btn-success" href="{{ url('/dashboard/export-donations') }}"
               title="PDF with Avery 5160 address labels" target="_blank"><i class="fas fa-file-excel"></i> Export
                Donations</a>
        </div>
    </div>
    <div class="row">
    <!--
		<div class="col-3">
			<a class="btn btn-success" href="{{ url('/dashboard/add-donation') }}"><i class="fas fa-plus"></i> Add donation</a>
		</div>
		-->
    </div>
    <p></p>
    <div class="row">
        <div class="col-12">
            <!-- STACKED BAR CHART -->
            <div class="card card-success">
                <!--
              <div class="card-header">
                <h3 class="card-title">Stacked Bar Chart</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button>
                </div>
              </div>
                -->
                <div class="card-body">
                    <div class="chart">
                        <canvas id="stackedBarChart" style="height:230px; min-height:230px"></canvas>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
            <!-- /.card -->
        </div>
    </div>
    <div class="row d-none">
        <div class="col-12">
            <h3>Report</h3>
            <form class="form-inline" method="GET" action="{{ route('donations') }}">
                <input type="hidden" name="filter" value="date">
                <div class="form-group">
                    <label for="from">From: </label>
                    <input type="date" id="from" class="form-control mx-sm-3" name="from"
                           value="{{ \Carbon\Carbon::create('2019', '09', '01')->format('Y-m-d') }}">
                </div>
                <div class="form-group">
                    <label for="from">To: </label>
                    <input type="date" id="to" class="form-control mx-sm-3" name="to"
                           value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}">
                </div>
                <div class="form-group">
                    <label for="period">Period: </label>
                    <input type="radio" id="periodWeekly" class="form-control mx-sm-3" name="period" value="weekly"
                           checked>Weekly
                    <input type="radio" id="periodMonthly" class="form-control mx-sm-3" name="period" value="monthly">Monthly
                </div>
                <div class="form-group">
                    <input type="submit" class="btn btn-success form-control mx-sm-3" value="Report">
                </div>
            </form>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <form method="post" action="{{ url('dashboard/store-donation')}}" autocomplete="off">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-12 col-md-8">
                                <div class="form-group">
                                    <label for="name">Donor Name</label>
                                    <select id="name" class="form-control donors-select select2"
                                            style="width: 100%; height: 15px" name="donor" autocomplete="off">
{{--                                        @foreach(\App\Models\Donor::all() as $donor)--}}
{{--                                            <option value="{{$donor->id}}">{{$donor->name}}</option>--}}
{{--                                        @endforeach--}}
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-4">
                                <div class="form-group">
                                    <label for="date">Date</label>
                                    <input id="date" type="date" class="form-control w-100" name="date"
                                           value="{{ \Carbon\Carbon::now(Config::get('TIMEZONE'))->format('Y-m-d') }}">
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-12 col-md-4 order-md-12">
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <select id="type" name="type" class="form-control">
                                        <option value="paypal">PayPal</option>
                                        <option value="mail" selected>Mail</option>
                                        <option value="personal">Personal</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12 col-md-8 order-md-1">
                                <div class="d-block d-md-none">
                                    <h4 class="mt-4">Categories</h4>
                                    <hr>
                                </div>
                                <div class="form-group row">
                                    @foreach(\App\Models\DonationsCategories::where('active', '=', 1)->get() as $category)
                                        <div class="col-12 col-md-4 col-xl-3 form-group">
                                            <label>{{ $category->name}}</label>
                                            <input name="categoryId[{{$category->id}}]" type="number"
                                                   class="form-control">
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group text-right">
                                    <button type="submit" class="btn btn-success add-donations"
                                            disabled><i class="fa fa-plus"></i> Add
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <p></p>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="width: 10px">#</th>
                            <th>Name</th>
                            <th>Donation</th>
                            <th>Category</th>
                            <th>Type</th>
                            <th>Date</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($donations as $donation)
                            <tr data-id="{{ $donation->id }}">
                                <td>{{ $donation->id }}</td>
                                <td data-field="name" data-donor-id="{{$donation->donor->id}}"><a
                                        href="{{ url('dashboard/donor')}}/{{ $donation->donor->id }}"><span>{{ $donation->donor->name }}</span></a>
                                </td>
                                <td data-field="amount"><span>{{ $donation->amount }}</span></td>
                                <td data-field="category"><span>{{ $donation->category->name }}</span></td>
                                <td data-field="type"><span>{{ $donation->type }}</span></td>
                                <td data-field="date">
                                    <span>{{ $donation->date->format('Y-m-d') }}</span>
                                    <a href="#"
                                       class="float-right action-donations-{{ $donation->id }} edit-donations invisible"
                                       data-id="{{ $donation->id }}"><i class="fa fa-edit"></i></a>
                                    <a href="{{ url('/dashboard/delete-donation') }}/{{ $donation->id }}"
                                       class="float-right mr-1 action-donations-{{ $donation->id }} invisible"><i
                                            class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('page_script')
    <script>
        (function() {
            let allDonors = [];

            function initDonorSelect($selector, id) {
                const $select = ($selector || $('.donors-select'));
                if (allDonors.length === 0) {
                    $.get('/api/donors', function (data) {
                        // cache value
                        allDonors = data;
                        setupSelect2()
                    }, 'json');
                } else {
                    setupSelect2()
                }

                function setupSelect2() {
                    $select.select2({
                        theme: 'bootstrap4',
                        placeholder: 'Select a donor',
                        templateResult: function (state) {
                            if (!state.full_address) {
                                return state.text;
                            }
                            return $(`<span>${state.name}</span><br><span>${state.full_address}</span>`);
                        },
                        data: allDonors.map(x => {
                            return {
                                id: x.id,
                                text: x.name,
                                name: x.name,
                                full_address: x.full_address,
                            }
                        }),
                    });
                }

                if (id) {
                    $select.val(id).trigger('change');
                }
                setTimeout(function() {
                    $select.val(null).trigger('change');
                }, 500)
            }

            function initDonationChart() {
                // Chart
                var areaChartData = {
                    labels: {!! json_encode($months) !!},
                    datasets: [
                        {
                            label: 'Mail',
                            backgroundColor: 'rgba(60,141,188,0.9)',
                            borderColor: 'rgba(60,141,188,0.8)',
                            pointRadius: false,
                            pointColor: '#3b8bba',
                            pointStrokeColor: 'rgba(60,141,188,1)',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(60,141,188,1)',
                            data: {!! json_encode($chart['mail']) !!}
                        },
                        {
                            label: 'PayPal',
                            backgroundColor: 'rgba(210, 214, 222, 1)',
                            borderColor: 'rgba(210, 214, 222, 1)',
                            pointRadius: false,
                            pointColor: 'rgba(210, 214, 222, 1)',
                            pointStrokeColor: '#c1c7d1',
                            pointHighlightFill: '#fff',
                            pointHighlightStroke: 'rgba(220,220,220,1)',
                            data: {!! json_encode($chart['paypal']) !!}
                        },
                        {
                            label: 'Personal',
                            backgroundColor: 'rgba(310, 214, 222, 1)',
                            borderColor: 'rgba(310, 214, 222, 1)',
                            pointRadius: false,
                            pointColor: 'rgba(310, 214, 222, 1)',
                            pointStrokeColor: '#c1c7d8',
                            pointHighlightFill: '#ffa',
                            pointHighlightStroke: 'rgba(320,220,220,1)',
                            data: {!! json_encode($chart['personal']) !!}
                        },
                    ]
                }

                var stackedBarChartCanvas = $('#stackedBarChart').get(0).getContext('2d')
                var stackedBarChartData = jQuery.extend(true, {}, areaChartData)

                var stackedBarChartOptions = {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        xAxes: [{
                            stacked: true,
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                    }
                }

                var stackedBarChart = new Chart(stackedBarChartCanvas, {
                    type: 'bar',
                    data: stackedBarChartData,
                    options: stackedBarChartOptions
                })
            }

            function initDonorsTable() {
                const $body = $('body');
                // редактирование пожертвований
                $body.on('mouseover', 'tr', function (e) {
                    let $that = $(this),
                        id = $that.attr('data-id');
                    $that.find('.action-donations-' + id + '').removeClass('invisible');
                }).on('mouseout', 'tr', function (e) {
                    let $that = $(this),
                        id = $that.attr('data-id');
                    $that.find('.action-donations-' + id + '').addClass('invisible');
                });

                $body.on('click', 'a.edit-donations', function (e) {
                    e.preventDefault();
                    var $that = $(this),
                        id = $that.attr('data-id');
                    var $editor = $('<tr data-id="' + id + '" data-type="edit"><td>' + id + '</td></tr>');
                    $that.parents('tr[data-id]').find('td').each(function () {
                        let $that = $(this),
                            field = $that.attr('data-field'),
                            text = $that.text().trim();

                        if (field === 'name') {
                            $editor.append('<td><select class="form-control select2" style="width: 100%;" name="donorEdit"></select></td>');
                            initDonorSelect($editor.find('select[name="donorEdit"]'), $that.attr('data-donor-id'));
                        } else if (field === 'amount') {
                            $editor.append('<td><input class="form-control" type="number" name="amountEdit" value="' + text + '"></td>');
                        } else if (field === 'category') {
                            let categories = [];
                            $body.find('input[name^="categoryId"]').parent().find('label').each(function () {
                                categories.push($(this).text());
                            });
                            $editor.append('<td><select class="form-control" style="width: 100%;" name="categoryEdit"></select></td>');
                            categories.forEach(function (category) {
                                if (text === category) {
                                    $editor.find('select[name="categoryEdit"]').append('<option selected>' + category + '</option>');
                                } else {
                                    $editor.find('select[name="categoryEdit"]').append('<option>' + category + '</option>');
                                }
                            });
                        } else if (field === 'type') {
                            const types = ['mail', 'personal', 'paypal'];
                            //$(this).html('<select class="form-control" style="width: 100%;" name="typeEdit"></select>');
                            $editor.append('<td><select class="form-control" style="width: 100%;" name="typeEdit"></select></td>');
                            types.forEach(function (type) {
                                if (text === type) {
                                    $editor.find('select[name="typeEdit"]').append('<option selected>' + type + '</option>');
                                } else {
                                    $editor.find('select[name="typeEdit"]').append('<option>' + type + '</option>');
                                }
                            });
                        } else if (field === 'date') {
                            text = text.trim();
                            $editor.append('<td><input type="date" class="form-control" name="dateEdit" value="' + text + '"><p></p><a href="#" class="btn btn-success mr-1 edit-save" data-id="' + id + '">Save</a><a href="#" class="btn btn-danger edit-cancel" data-id="' + id + '">Cancel</a></td>');
                        }
                    });
                    $that.parents('tr[data-type!="edit"]').before($editor).addClass('d-none');
                });

                $body.on('click', 'a.edit-cancel', function (e) {
                    e.preventDefault();
                    var $that = $(this);
                    $that.parents('tr[data-id]').siblings('.d-none').removeClass('d-none');
                    $that.parents('[data-type="edit"]').remove();
                    //location.reload();
                });

                $body.on('click', 'a.edit-save', function (e) {
                    e.preventDefault();
                    const payload = {
                        'id': $(this).attr('data-id'),
                        'donorId': $('select[name="donorEdit"]').val(),
                        'amount': $('input[name="amountEdit"]').val(),
                        'category': $('select[name="categoryEdit"]').val(),
                        'type': $('select[name="typeEdit"]').val(),
                        'date': $('input[name="dateEdit"]').val(),
                    }

                    $.post('edit-donations', payload, function (data) {
                        if (data == 'edit-donations') {
                            location.reload();
                        }
                    });
                });

                // показать количество доноров с учетом фильтрации
                $('.amount-donors').html($('tr.donor').not('.d-none').length);

                // указание тэгов если есть параметр include
                let link = location.href;
                link = link.split('?include=');
                if (link.length > 1) {
                    link[1] = decodeURI(link[1]);
                    $('tr.donor').addClass('d-none');
                    $('tr[data-tags*="' + link[1] + '"]').removeClass('d-none');
                    $('select[name="include"] option[value="' + link[1] + '"]').prop('selected', true);
                    $('select[name="exclude"] option').removeAttr('selected');
                }
                // =====
            }

            $(document).ready(function () {
                initDonationChart();
                initDonorSelect();
                initDonorsTable();
            })
        })()
    </script>
@endsection
