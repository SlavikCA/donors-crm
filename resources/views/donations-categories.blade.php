@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-12">
			<div class="card">
				
				<div class="card-body">
		<table class="table table-bordered">
			<thead>
				<tr>
                    <th style="width: 10px">#</th>
                    <th>Category</th>
                    <th>Actions</th>
                </tr>
			</thead>
			<tbody>
				<tr>
					<form method="POST" action="{{ url('dashboard/add-donations-categories') }}">
						{{ csrf_field() }}
					<td></td>
					<td><input class="form-control" type="text" name="category"></td>
					<td><button type="submit" class="btn btn-success"><i class="fa fa-plus"></i> Add</button></td>
					</form>
				</tr>
				@foreach($categories as $category)
					<tr>
						<td>{{ $category->id }}</td>
						<td>@if($category->active == 0)<span class="font-italic text-muted">{{ $category->name }}</span>@else<span>{{ $category->name }}</span>@endif</td>
						<td>
							<a href="donations-categories-toggle-active/{{ $category->id }}?active={{ $category->active }}"><i class="fas @if($category->active == 0) fa-eye @else fa-eye-slash @endif"></i></a>
						</td>
					<tr>
				@endforeach
			</tbody>
		</table>
				</div>
			</div>
		</div>
	</div>
@endsection