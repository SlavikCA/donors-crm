@extends('layouts.dashboard')

@section('content')
	<div class="row">
		<div class="col-12">
			Donors: {{ \App\Models\Donor::count() }}
		</div>
		<div class="col-12">
			<h3>Latest 5 donors:</h3>
			@foreach(\App\Models\Donor::orderBy('created_at', 'desc')->limit(5)->get() as $donor)
				<div>{{ $donor->name }}</div>
			@endforeach
		</div>
		<div class="col-12">
			<h3>Latest 5 donations:</h3>
			<div class="card">
				<div class="card-body">
					<table class="table table-bordered">
						<thead>
							<tr>
								<th style="width: 10px">#</th>
								<th>Donor</th>
								<th>Donation</th>
							</tr>
						</thead>
						<tbody>
						@foreach(\App\Models\Donation::orderBy('created_at', 'desc')->limit(5)->get() as $donation)
							<tr>
								<td>{{ $loop->iteration }}</td>
								<td>{{ $donation->donor->name }}</td>
								<td>{{ $donation->amount }}</td>
						@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
