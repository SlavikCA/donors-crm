<?php

namespace App\Http\Services;

use App\Models\Tag;

class TagsService
{
    public function getTagsSortedBy(?string $sortBy = null)
    {
        if ($sortBy === 'name') {
            return Tag::orderBy('name')->get();
        }

        // sort by amount

        // grab ids to avoid include all column in group by
        return Tag::selectRaw('tags.*, COUNT(DISTINCT donors.id) as donor_amount')
            ->leftJoin('donors_tags', 'tags.id', '=', 'donors_tags.tag_id')
            ->leftJoin('donors', 'donors_tags.donor_id', '=', 'donors.id')
            ->groupBy('tags.id')
            ->orderBy('donor_amount', 'DESC')
            ->groupBy('tags.id', 'tags.name', 'tags.description', 'tags.is_system')
            ->get();
    }
}
