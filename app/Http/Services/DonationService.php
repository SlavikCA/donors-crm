<?php

namespace App\Http\Services;

use App\Models\Donation;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class DonationService
{
    public function getDonationYearRange(): array
    {
        $years = DB::select('SELECT MIN(date) as minDate, MAX(date) as maxDate FROM donations');
        $currentYear = now()->year;
        if (empty($years)) {
            return [$currentYear, $currentYear];
        }

        return [Carbon::parse($years[0]->minDate)->year, Carbon::parse($years[0]->maxDate)->year];
    }

    public function getDonationChartData(Carbon $from, Carbon $to, array $types = []): array
    {
        $monthNames = [];
        for ($i = 1; $i <= 12; $i++) {
            $monthNames[] = now()->setMonth($i)->englishMonth;
        }
        unset($i);

        if (empty($types)) {
            $types = ['personal', 'paypal', 'mail'];
        }

        $chart = [];
        $monthDiff = $from->diffInMonths($to);
        foreach ($types as $type) {
            $monthStart = $from->copy()->firstOfMonth();
            for ($ix = 0; $ix <= $monthDiff; $ix++) {
                $monthEnd = $monthStart->copy()->endOfMonth();
                $chart[$type][] = Donation::whereBetween('date', [
                    $monthStart,
                    $monthEnd,
                ])->whereType($type)->sum('amount');
                $monthStart = $monthStart->addMonthNoOverflow()->firstOfMonth();
            }
        }

        return [
            'labels' => $monthNames,
            'data' => $chart,
        ];
    }
}
