<?php

namespace App\Http\Services;

use App\Models\Donation;
use App\Models\Donor;
use App\Models\Tag;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\DB;

class DonorsService
{
    public function checkDonorsRecentTag(?Donor $donor = null): int
    {
        $recentBefore = now()->subHours(12);
        /** @var Tag $tag */
        $tag = Tag::whereName('added in last 24hrs')->firstOrFail();

        if (! isset($donor)) {
            $donors = Donor::select('donors.*')
                ->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->leftJoin('tags', 'donors_tags.tag_id', '=', 'tags.id')
                ->where('created_at', '>', now()->subHour())
                ->orWhere('tags.name', '=', $tag->name)
                ->get();
        } else {
            $donors = new Collection([$donor]);
        }

        /** @var Donor $donor */
        foreach ($donors as $donor) {
            if ($donor->created_at->gte($recentBefore)) {
                $donor->tags()->syncWithoutDetaching([$tag->id]);
            } else {
                $donor->tags()->detach($tag->id);
            }
            $donor->save();
        }

        return $donors->count();
    }

    public function checkDonorsNoContactIn18MonthTag(?Donor $donor = null): int
    {
        $staleAfter = now()->subDays(18 * 30);

        /** @var Tag $tag */
        $staleTag = Tag::whereName('no contacts in 18months')->firstOrFail();
        $activeDonorTag = Tag::whereName('active donor')->firstOrFail();

        if (! isset($donor)) {
            // just grab ids instead of adding all donor columns to groupBy
            $donorIds = Donor::selectRaw('donors.id, max(donations.created_at)')
                ->leftJoin('donations', 'donors.id', '=', 'donations.donor_id')
                ->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->leftJoin('tags', 'donors_tags.tag_id', '=', 'tags.id')
                ->where(function ($q) use ($staleAfter) {
                    return $q->whereNull('donations.created_at')
                        ->orWhere('donations.created_at', '>', $staleAfter);
                })
                ->where(function ($q) use ($staleTag) {
                    $q->whereNull('tags.name')
                        ->orWhere(function ($q) use ($staleTag) {
                            $q->where('tags.name', '=', $staleTag->name)->whereNotNull('donations.id');
                        });
                })
                ->groupBy('donors.id')
                ->get()
                ->pluck('id')
                ->toArray();

            $donors = Donor::whereIn('id', $donorIds)->get();
        } else {
            $donors = new Collection([$donor]);
        }

        /** @var Donor $donor */
        foreach ($donors as $donor) {
            if ($donor->donations()->where('created_at', '<', $staleAfter)->count()) {
                $donor->tags()->detach($staleTag->id);
                $donor->tags()->syncWithoutDetaching([$activeDonorTag->id]);
            } else {
                $donor->tags()->detach($activeDonorTag->id);
                $donor->tags()->syncWithoutDetaching([$staleTag->id]);
            }
            $donor->save();
        }

        return $donors->count();
    }

    public function getDonorActivityLog(Donor $donor)
    {
        $activity = [];
        $donations = Donation::whereDonorId($donor->id)->with('category')->get();
        foreach ($donations as $donation) {
            $activity[] = [
                'date' => $donation->date->format('Y-m-d'),
                'type' => 'DONATION',
                'data' => $donation,
            ];
        }
        $comments = $donor->comments;
        foreach ($comments as $comment) {
            $activity[] = [
                'date' => $comment->created_at->format('Y-m-d'),
                'type' => 'COMMENT',
                'data' => $comment,
            ];
        }

        usort($activity, function ($a, $b) {
            return $a['date'] <=> $b['date'];
        });
        // technically not needed if usort conditional is appropriate
        $activity = array_reverse($activity);

        return $activity;
    }

    /**
     * Softdelete donor and disassociate tags
     */
    public function deleteDonor($id)
    {
        $donor = Donor::findOrFail($id);
        DB::table('donors_tags')->where('donor_id', $id)->delete();
        $donor->delete();
    }
}
