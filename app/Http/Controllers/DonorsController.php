<?php

namespace App\Http\Controllers;

use App\Http\Resources\DonorResource;
use App\Models\Donor;
use Illuminate\Http\Request;

class DonorsController extends Controller
{
    public function index(Request $request)
    {
        if ($q = $request->query('q')) {
            $data = Donor::where('name', 'like', "%$q%")
                ->orWhere('street', 'like', "%$q%")
                ->orWhere('state', 'like', "%$q%")
                ->orWhere('city', 'like', "%$q%")
                ->orWhere('email', 'like', "%$q%")
                ->get();
        } else {
            $data = Donor::with(['tags'])->get();
        }

        return DonorResource::collection($data);
    }

    public function show($id, Request $request)
    {
        return DonorResource::make(Donor::findOrFail($id));
    }
}
