<?php

namespace App\Http\Controllers;

use App\Models\DonationsCategories;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class DonationsController extends Controller
{
    public function categories(Request $request)
    {
        return new JsonResponse(DonationsCategories::all(), 200, ['Content-Type' => 'application/json'], true);
    }
}
