<?php

namespace App\Http\Controllers;

use App\Exports\Donations as HackeryDonations;
use App\Exports\Donors as HackeryDonors;
use App\Http\Services\DonationService;
use App\Http\Services\DonorsService;
use App\Http\Services\TagsService;
use App\Models\Comment;
use App\Models\Donation;
use App\Models\DonationsCategories;
use App\Models\Donor;
use App\Models\Tag;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Maatwebsite\Excel\Facades\Excel;

class DashboardController extends Controller
{
    private DonorsService $donorService;

    private DonationService $donationService;

    private TagsService $tagsService;

    public function __construct(DonationService $donationService, DonorsService $donorService, TagsService $tagsService)
    {
        $this->donationService = $donationService;
        $this->donorService = $donorService;
        $this->tagsService = $tagsService;
    }

    //
    public function getDonors(Request $request)
    {
        $donorsQuery = Donor::select('donors.*')
            ->with('tags')
            ->orderBy('donors.updated_at', 'desc');
        // replace with client side filtering
        //        if ($request->query('include')) {
        //            $donorsQuery->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
        //                ->leftJoin('tags', 'donors_tags.tag_id', '=', 'tags.id')
        //                ->whereIn('tags.name', [explode(',', $request->query('include'))]);
        //        }
        $donors = $donorsQuery->get();
        $countDonors = $donors->count();

        $year = $request->query->has('year') ? (int) $request->query('year') : now()->year;
        $yearStart = Carbon::createFromDate($year, 1, 1)->setTime(0, 0, 0);
        $yearEnd = $yearStart->copy()->endOfYear();

        //        $donations = Donation::whereBetween('date', [$yearStart, $yearEnd])->orderBy('date', 'desc')->get();
        $donationRange = $this->donationService->getDonationYearRange();
        if ($donationRange[1] < $year) {
            // adjust donation range to show current year if no donations were done in current year
            $donationRange[1] = $year;
        }
        $chartData = $this->donationService->getDonationChartData($yearStart, $yearEnd);

        return view('donors', [
            'page' => 'Donors',
            'countDonors' => $countDonors,
            'donors' => $donors,
            //            'donations' => $donations,
            'months' => $chartData['labels'],
            'chart' => $chartData['data'],
            'donationRange' => $donationRange,
            'tags' => Tag::all(),
        ]);
    }

    /*
    public function editDonor($id)
    {
        $donor = Donor::find($id);
        return view('edit-donor', ['page'=>'Edit Donor', 'donor'=>$donor]);
    }
    */

    public function storeDonor(Request $request)
    {
        $data = $request->all();
        //dd($data);
        if ($data['action'] == 'edit') {
            $donor = Donor::findOrFail($data['id']);
            $arrDonor = $donor->toArray();
            $arrDonor['tags'] = $donor->tags->pluck('name')->toArray();
            // создание комментариев
            $arrEditFields = explode(' ', $data['editfields']);
            $arrEditFields = array_unique($arrEditFields);
            if ($data['editfields'] != null) {
                $comment = 'User '.Auth::user()->name.' made changes:<br>';
            }
            foreach ($arrEditFields as $editField) {
                $editField = trim($editField, '[]');
                if ($editField == 'tags') {
                    if (isset($data['tags'])) {
                        $comment .= 'Changed tags to: '.implode(', ', $data['tags']);
                    } else {
                        $comment .= 'Deleted tags: '.implode(', ', $arrDonor['tags'] ?? []);
                    }

                    continue;
                } else {
                    if (isset($arrDonor[$editField])) {
                        $comment .= 'Changed '.$editField.' from "'.$arrDonor[$editField].'" to "'.$data[$editField].'"<br>';
                    }
                }
            }
            if ($data['editfields'] != null) {
                $commentModel = new Comment;
                $commentModel->donor_id = $data['id'];
                $commentModel->comment = $comment;
                $commentModel->save();
            }
        }
        if ($data['action'] == 'add') {
            $donor = new Donor;
        }
        $donor->name = $data['name'];
        $donor->street = $data['street'];
        $donor->city = $data['city'];
        $donor->state = $data['state'];
        $donor->zip = $data['zip'];
        if ($data['email'] != null) {
            $donor->email = $data['email'];
        }
        if ($data['phone'] != null) {
            $donor->phone = $data['phone'];
        }
        $donor->save();
        if ($data['action'] == 'add') {
            $tagIds = [Tag::where('name', 'added in last 24hrs')->firstOrFail()->id];
            foreach ($data['tags'] ?? [] as $tag) {
                $tagIds[] = Tag::where('name', $tag)->firstOrFail()->id;
            }
            $donor->tags()->syncWithoutDetaching($tagIds);

        }
        if ($data['action'] == 'edit') {
            $tagIds = [];
            foreach ($data['tags'] ?? [] as $tag) {
                $tagIds[] = Tag::where('name', $tag)->firstOrFail()->id;
            }
            $donor->tags()->sync($tagIds);
        }
        if ($data['action'] == 'add') {
            return redirect()->route('donors');
        }
        if ($data['action'] == 'edit') {
            return redirect()->back();
        }

    }

    public function deleteDonor($id)
    {
        $this->donorService->deleteDonor($id);

        return redirect()->route('donors');
    }

    public function getDonations(Request $request)
    {
        $year = $request->query->has('year') ? (int) $request->query('year') : now()->year;
        $yearStart = Carbon::createFromDate($year, 1, 1)->setTime(0, 0, 0);
        $yearEnd = $yearStart->copy()->endOfYear();

        $donations = Donation::whereBetween('date', [$yearStart, $yearEnd])->orderBy('date', 'desc')->get();
        $donationRange = $this->donationService->getDonationYearRange();
        if ($donationRange[1] < $year) {
            // adjust donation range to show current year if no donations were done in current year
            $donationRange[1] = $year;
        }
        $chartData = $this->donationService->getDonationChartData($yearStart, $yearEnd);

        return view('donations', [
            'donations' => $donations,
            'page' => 'Donations',
            'months' => $chartData['labels'],
            'chart' => $chartData['data'],
            'donationRange' => $donationRange,
        ]);
    }

    public function storeDonation(Request $request)
    {
        $data = $request->all();
        if ($data['date'] == null) {
            $date = Carbon::now()->format('Y-m-d');
        } else {
            $date = $data['date'];
        }
        if (isset($data['action'])) {
            if ($data['action'] == 'add-donor') {
                $donor = new Donor;
                $donor->name = $data['name'];
                $donor->address = $data['address'];
                $donor->save();
                $donation = new Donation;
                $donation->donor_id = $donor->id;
                $donation->date = $date;
                $donation->amount = $data['amount'];
                $donation->category_id = $data['categoryId'];
                $donation->type = $data['type'];
                $donation->save();
            }
        } else {
            foreach ($data['categoryId'] as $categoryId => $amount) {
                if ($amount == '' || $amount <= 0) {
                    continue;
                }

                /** @var Donor $donor */
                $donor = Donor::findOrFail($data['donor']);

                $donation = new Donation;
                $donation->donor_id = $donor->id;
                $donation->date = $date;
                $donation->amount = $amount;
                $donation->category_id = $categoryId;
                $donation->type = $data['type'];
                $donation->save();

                $tagNoContactsIn18Months = Tag::where('name', 'no contacts in 18months')->firstOrFail();
                $tagActiveDonor = Tag::where('name', 'active donor')->first();
                $donor->tags()->detach($tagNoContactsIn18Months->id);
                $donor->tags()->syncWithoutDetaching([$tagActiveDonor->id]);
            }
        }

        return redirect()->route('donations');
    }

    public function getDonor($id)
    {
        $donor = Donor::findOrFail($id);
        $activity = $this->donorService->getDonorActivityLog($donor);

        return view('donor-donations', [
            'activity' => $activity,
            'page' => 'Donor',
            'donor' => $donor,
            'donor_tag_names' => $donor->tags->pluck('name')->toArray(),
        ]);
    }

    public function getPdf(Request $request)
    {
        $idTagInclude = [];
        $idTagExclude = [];
        $arrInclude = array_filter(explode(',', trim($request->query('include'))));
        foreach ($arrInclude as $tag) {
            /** @var Tag $tag */
            $tag = Tag::whereName($tag)->firstOrFail();
            $idTagInclude = array_merge($idTagInclude, $tag->donors->pluck('id')->toArray());
        }
        $idTagInclude = array_unique($idTagInclude);

        $arrExclude = array_filter(explode(',', trim($request->query('exclude'))));
        foreach ($arrExclude as $tag) {
            /** @var Tag $tag */
            $tag = Tag::whereName($tag)->firstOrFail();
            $idTagExclude = array_merge($idTagExclude, $tag->donors->pluck('id')->toArray());
        }
        $idTagExclude = array_unique($idTagExclude);

        if (! $idTagInclude) {
            $donorsInclude = DB::table('donors')
                ->select('donors.*')
                ->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->where('donors_tags.donor_id', '=', null)
                ->whereNull('donors.deleted_at')
                ->orderBy('donors.zip')
                ->distinct();
            $donorsInclude = DB::table('donors')
                ->select('donors.*')
                ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->whereNull('donors.deleted_at')
                ->orderBy('donors.zip')
                ->union($donorsInclude)
                ->distinct()
                ->get();
        } else {
            $donorsInclude = DB::table('donors')
                ->select('donors.*')
                ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->whereIn('donors_tags.donor_id', $idTagInclude)
                ->whereNull('donors.deleted_at')
                ->orderBy('donors.zip')
                ->distinct()
                ->get();
        }

        $donorsExclude = DB::table('donors')
            ->select('donors.*')
            ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
            ->whereIn('donors_tags.donor_id', $idTagExclude)
            ->whereNull('donors.deleted_at')
            ->orderBy('donors.zip')
            ->distinct()
            ->get();
        $donorsInclude = $donorsInclude->toArray();
        $arrDonorsInclude = [];
        foreach ($donorsInclude as $donorInclude) {
            $arrDonorsInclude[$donorInclude->id] = (array) $donorInclude;
        }
        $donorsExclude = $donorsExclude->toArray();
        $arrDonorsExclude = [];
        foreach ($donorsExclude as $donorExclude) {
            $arrDonorsExclude[$donorExclude->id] = (array) $donorExclude;
        }
        $donors = array_diff_key($arrDonorsInclude, $arrDonorsExclude);

        $lists = [];
        $donorsLists = array_chunk($donors, 30);
        foreach ($donorsLists as $key => $donorsList) {
            $donorsList = array_chunk($donorsList, 3);
            foreach ($donorsList as $row) {
                $lists[$key][] = $row;
            }
        }
        $data['lists'] = $lists;
        $pdf = PDF::loadView('pdf-donors', $data);

        return $pdf->stream();
    }

    public function getUsers()
    {
        $users = User::all();

        return view('users', ['users' => $users, 'page' => 'Users']);
    }

    public function toggleUsersActive(Request $request, $id)
    {
        $user = User::find($id);
        $user->active = $request->query->get('active') == 'true';
        $user->save();

        return redirect()->route('users');
    }

    public function getDonationsCategories()
    {
        $categories = DonationsCategories::all();

        return view('donations-categories', ['categories' => $categories, 'page' => 'Donations Categories']);
    }

    public function addDonationsCategories(Request $request)
    {
        $data = $request->all();
        $category = new DonationsCategories;
        $category->name = $data['category'];
        $category->active = 1;
        $category->save();

        return redirect()->route('donations-categories');
    }

    public function toggleDonationsCategoriesActive(Request $request, $id)
    {
        $category = DonationsCategories::find($id);
        if ($request->query('active') == 0) {
            $category->active = 1;
        } else {
            $category->active = 0;
        }
        $category->save();

        return redirect()->route('donations-categories');
    }

    public function autocomplete(Request $request)
    {
        $data = $request->all();
        $strings = DB::table('donors')
            ->select($data['name'].' as name')
            ->distinct()
            ->where($data['name'], 'like', "{$data['string']}%")
            ->get();
        $strings = $strings->toJson();

        return $strings;
    }

    public function editDonations(Request $request)
    {
        $data = $request->all();
        $donorId = Donor::findOrFail($data['donorId']);
        $categoryId = DonationsCategories::where('name', '=', $data['category'])->first();
        $donations = Donation::where('id', '=', $data['id'])->update([
            'donor_id' => $donorId->id,
            'date' => $data['date'],
            'amount' => $data['amount'],
            'type' => $data['type'],
            'category_id' => $categoryId->id,
        ]);

        return 'edit-donations';
    }

    public function deleteDonation(Request $request, $id)
    {
        $donation = Donation::destroy($id);

        return redirect()->route('donations');
    }

    public function exportDonors(Request $request)
    {
        $include = $request->query('include');
        $exclude = $request->query('exclude');

        return Excel::download(new HackeryDonors($include, $exclude), 'donors.xlsx');
    }

    public function exportDonations()
    {
        return Excel::download(new HackeryDonations, 'donations.xlsx');
    }

    public function getCategoriesReport(Request $request)
    {
        $colors = [
            '#c56476',
            '#6f77cc',
            '#14c59c',
            '#14c519',
            '#c2c514',
            '#c55e14',
            '#f5c4c4',
            '#f9066b',
            '#06f967',
            '#063af9',
            '#f91c06',
            '#f55858',
            '#eb58f5',
            '#77f558',
            '#585df5',
            '#62f558',
            '#58f5f5',
        ];
        if ($request->query('year')) {
            $year = $request->query('year');
        } else {
            $year = Carbon::now()->year;
        }
        //$donations = Donation::orderBy('date', 'desc')->get();
        $countDonations = Donation::where('date', 'LIKE', "%$year%")->orderBy('date', 'desc')->count();
        if ($countDonations == 0) {
            $year = $year - 1;
        }
        $categories = DonationsCategories::all();
        $months = [];
        $table = [];
        for ($i = 1; $i < 13; $i++) {
            //$months['names'][] = Carbon::now()->subMonths($i)->englishMonth;
            $months['numbers'][] = Carbon::create($year)->subMonths($i)->month;
            $months['daysInMonth'][] = Carbon::create($year)->subMonths($i)->daysInMonth;
            //$months['years'][] = Carbon::create($year)->subMonths($i)->year;
            $months['years'][] = Carbon::create($year)->year;
            //$sumDonations = Donation::whereBetween('date', [Carbon::create($year)->subMonths($i)->year.'-'.Carbon::create($year)->subMonths($i)->month.'-01', Carbon::create($year)->subMonths($i)->year.'-'.Carbon::create($year)->subMonths($i)->month.'-'.Carbon::create($year)->subMonths($i)->daysInMonth])->sum('amount');
            $sumDonations = Donation::whereBetween('date', [
                Carbon::create($year)->year.'-'.Carbon::create($year)->subMonths($i)->month.'-01',
                Carbon::create($year)->year.'-'.Carbon::create($year)->subMonths($i)->month.'-'.Carbon::create($year)->subMonths($i)->daysInMonth,
            ])->sum('amount');
            $months['sumDonationsMonth'][] = $sumDonations;
            $months['names'][] = Carbon::create($year)->subMonths($i)->englishMonth.' '.$sumDonations;
        }
        //dd($months['years']);
        $monthsNames = array_reverse($months['names']);
        //dd($monthsNames);
        $sumDonations = array_reverse($months['sumDonationsMonth']);
        //$types = ['personal', 'paypal', 'mail'];
        foreach ($categories as $category) {
            $types[$category->id] = $category->name;
            //$table[$category->id]['name'] = $category->name;
        }
        $chart = [];
        foreach ($types as $keyType => $type) {
            foreach ($months['numbers'] as $key => $month) {
                //$chartValues = Donation::whereBetween('date', [$months['years'][$key].'-'.$months['numbers'][$key].'-01', $months['years'][$key].'-'.$months['numbers'][$key].'-'.$months['daysInMonth'][$key]])->where('category_id', '=', $keyType)->sum('amount');
                $chart[$keyType][] = Donation::whereBetween('date', [
                    $months['years'][$key].'-'.$months['numbers'][$key].'-01',
                    $months['years'][$key].'-'.$months['numbers'][$key].'-'.$months['daysInMonth'][$key],
                ])->where('category_id', '=', $keyType)->sum('amount');

                //dd($chartValues);
            }
            $chart[$keyType] = array_reverse($chart[$keyType]);
        }
        $tableMonth = [];
        for ($i = 0; $i < Carbon::now()->month; $i++) {
            $tableMonth[] = Carbon::now()->subMonths($i)->englishMonth;
        }
        $tableMonth = array_reverse($tableMonth);

        $years = Donation::select('date')->orderBy('date')->get();
        $years = $years->toArray();
        $yearBegin = array_shift($years);
        $yearBegin = explode('-', $yearBegin['date']);
        $yearEnd = array_pop($years);
        $yearEnd = explode('-', $yearEnd['date']);

        return view('categories-report', [
            'page' => 'Categories Report',
            'months' => $monthsNames,
            'categories' => $categories,
            'chart' => $chart,
            'table' => $table,
            'tableMonth' => $tableMonth,
            'sumDonations' => $sumDonations,
            'yearBegin' => $yearBegin[0],
            'yearEnd' => $yearEnd[0],
            'colors' => $colors,
        ]);
    }

    public function getTags(Request $request)
    {
        $tagData = $this->tagsService->getTagsSortedBy($request->query('sort'));

        return view('tags', ['page' => 'Tags', 'tags' => $tagData]);
    }

    public function editTag(Request $request)
    {
        $data = $request->all();
        $tag = Tag::findOrFail($data['id']);
        if ($tag->is_system) {
            throw ValidationException::withMessages(['name' => ['Cannot modify system tags.']]);
        }
        $tag->update(['name' => $data['name'], 'description' => $data['description']]);
        $data = json_encode($data);

        return $data;
    }

    public function addTag(Request $request)
    {
        $data = $request->all();
        $tag = new Tag;
        $tag->name = $data['newname'];
        $tag->description = $data['newdescription'];
        $tag->save();

        return redirect('dashboard/tags');
    }

    public function deleteTag($id)
    {
        /** @var Tag $tag */
        $tag = Tag::findOrFail($id);
        if ($tag->is_system) {
            throw ValidationException::withMessages(['name' => ['Cannot modify system tags.']]);
        }

        $tag->donors()->detach($tag->donors->pluck('id')->toArray());
        $tag->delete();

        return redirect('dashboard/tags');
    }

    public function addComment(Request $request)
    {
        $data = $request->all();

        $modelComment = Comment::create([
            'donor_id' => $data['donor_id'],
            'comment' => htmlspecialchars($data['comment'] ?? ''),
        ]);

        //return redirect('dashboard/donor/'.$data['donor_id'].'');
        return redirect()->back();
    }

    public function getComment(Request $request)
    {
        return Comment::find($request->get('id'));
    }

    public function editComment(Request $request)
    {
        $data = $request->all();
        $comment = Comment::findOrFail($data['id']);
        $comment->comment = htmlspecialchars($data['comment'] ?? '');
        //$comment->created_at = Carbon::parse($data['date'])->format('Y-m-d');
        $comment->created_at = $data['date'];
        $comment->save();

        //return 'edit-comments';
        return $comment;
    }

    public function deleteComment($id)
    {
        Comment::destroy($id);

        //return redirect('dashboard/donors');
        return redirect()->back();
    }
}
