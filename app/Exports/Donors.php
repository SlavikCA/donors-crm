<?php

namespace App\Exports;

use App\Models\Donor;
//use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromArray;

class Donors implements FromArray
{
    public function __construct($include, $exclude)
    {
        $this->include = $include;
        $this->exclude = $exclude;
    }

    public function array(): array
    {
        /*
        if($this->include or $this->exclude){
            $idTagInclude = '';
            $idTagExclude = '';
            $arrInclude = explode(',', $this->include);
            foreach($arrInclude as $tag){
                if($tag == ''){
                    continue;
                }
                $idTag = \App\Models\Tag::where('name', $tag)->first();
                $idTagInclude .= $idTag->id . ',';
            }
            $arrExclude = explode(',', $this->exclude);
            foreach($arrExclude as $tag){
                if($tag == ''){
                    continue;
                }
                $idTag = \App\Models\Tag::where('name', $tag)->first();
                $idTagExclude .= $idTag->id . ',';
            }
            $donorsHasTags = DB::table('donors')
                                ->select('donors.*')
                                ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                                ->whereIn('donors_tags.tag_id', [$idTagInclude])
                                ->whereNotIn('donors_tags.tag_id', [$idTagExclude])
                                ->orderBy('donors.created_at', 'desc')
                                ->get();
            return $donorsHasTags;
        }
        else {
            $idTagInactive = \App\Models\Tag::where('name', 'inactive')->first();
            $donorsHasTags = DB::table('donors')
                                ->select('donors.*')
                                ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                                ->whereNotIn('donors_tags.tag_id', [$idTagInactive->id])
                                ->orderBy('donors.created_at', 'desc');
            $donors = DB::table('donors')
                                ->select('donors.*')
                                ->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                                ->where('donors_tags.donor_id', '=', null)
                                ->whereNull('donors.deleted_at')
                                ->orderBy('donors.created_at', 'desc')
                                ->union($donorsHasTags)
                                ->get();
            return $donors;
            */
        $idTagInclude = [];
        $idTagExclude = [];
        $arrInclude = explode(',', $this->include);
        if (count($arrInclude) >= 1 && $arrInclude[0] != '') {
            foreach ($arrInclude as $tag) {
                if ($tag == '') {
                    continue;
                }
                $idDonor = DB::table('donors_tags')
                    ->select('donors_tags.donor_id')
                    ->join('tags', 'tags.id', '=', 'donors_tags.tag_id')
                    ->where('tags.name', $tag)
                    ->get();
                foreach ($idDonor as $id) {
                    $idTagInclude[] = $id->donor_id;
                }
            }
        }

        $arrExclude = explode(',', $this->exclude);
        if (count($arrExclude) >= 1 && $arrExclude[0] != '') {
            foreach ($arrExclude as $tag) {
                if ($tag == '') {
                    continue;
                }
                $idDonor = DB::table('donors_tags')
                    ->select('donors_tags.donor_id')
                    ->join('tags', 'tags.id', '=', 'donors_tags.tag_id')
                    ->where('tags.name', $tag)
                    ->get();
                foreach ($idDonor as $id) {
                    $idTagExclude[] = $id->donor_id;
                }
            }
        }

        if (! $idTagInclude) {
            $donorsInclude = DB::table('donors')
                ->select('donors.*')
                ->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->where('donors_tags.donor_id', '=', null)
                ->whereNull('donors.deleted_at')
                ->orderBy('donors.zip')
                ->distinct();
            $donorsInclude = DB::table('donors')
                ->select('donors.*')
                ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->whereNull('donors.deleted_at')
                ->orderBy('donors.zip')
                ->union($donorsInclude)
                ->distinct()
                ->get();
        } else {
            $donorsInclude = DB::table('donors')
                ->select('donors.*')
                ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
                ->whereIn('donors_tags.donor_id', $idTagInclude)
                ->whereNull('donors.deleted_at')
                ->orderBy('donors.zip')
                ->distinct()
                ->get();
        }

        $donorsExclude = DB::table('donors')
            ->select('donors.*')
            ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
            ->whereIn('donors_tags.donor_id', $idTagExclude)
            ->whereNull('donors.deleted_at')
            ->orderBy('donors.zip')
            ->distinct()
            ->get();
        $donorsInclude = $donorsInclude->toArray();
        $arrDonorsInclude = [];
        foreach ($donorsInclude as $donorInclude) {
            $arrDonorsInclude[$donorInclude->id] = (array) $donorInclude;
        }
        $donorsExclude = $donorsExclude->toArray();
        $arrDonorsExclude = [];
        foreach ($donorsExclude as $donorExclude) {
            $arrDonorsExclude[$donorExclude->id] = (array) $donorExclude;
        }
        $donors = array_diff_key($arrDonorsInclude, $arrDonorsExclude);

        return $donors;
    }
}
