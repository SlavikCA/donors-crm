<?php

namespace App\Exports;

use App\Models\Tag;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\FromCollection;

class Donations implements FromCollection
{
    public function collection()
    {
        //return Donor::select('name', 'street', 'city', 'state', 'zip', 'church', 'donors_level', 'created_at')->whereNull('deleted_at')->get();
        /*
        return DB::table('donations')
                    ->select('donations.date', 'donations.amount', 'donations.type', 'donations_categories.name', 'donors.name', 'donors.street', 'donors.city', 'donors.state', 'donors.zip', 'donors.church', 'donors.donors_level')
                    ->join('donors', 'donors.id', '=', 'donations.donor_id')
                    ->join('donations_categories', 'donations_categories.id', '=', 'donations.category_id')
                    ->get();
        */
        $idTagInactive = Tag::where('name', 'inactive')->first();
        $donationsDonorsHasTags = DB::table('donations')
            ->select('donations.date', 'donations.amount', 'donations.type', 'donations_categories.name', 'donors.name', 'donors.street', 'donors.city', 'donors.state', 'donors.zip')
            ->join('donors', 'donors.id', '=', 'donations.donor_id')
            ->join('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
            ->join('donations_categories', 'donations_categories.id', '=', 'donations.category_id')
            ->whereNotIn('donors_tags.tag_id', [$idTagInactive->id]);
        //->get();
        //dd($donationsDonorsHasTags);
        $donationsDonors = DB::table('donations')
            ->select('donations.date', 'donations.amount', 'donations.type', 'donations_categories.name', 'donors.name', 'donors.street', 'donors.city', 'donors.state', 'donors.zip')
            ->join('donors', 'donors.id', '=', 'donations.donor_id')
            ->leftJoin('donors_tags', 'donors.id', '=', 'donors_tags.donor_id')
            ->join('donations_categories', 'donations_categories.id', '=', 'donations.category_id')
            ->where('donors_tags.donor_id', '=', null)
            ->whereNull('donors.deleted_at')
            ->orderBy('donors.created_at', 'desc')
            ->union($donationsDonorsHasTags)
            ->get();

        return $donationsDonors;
    }
}
