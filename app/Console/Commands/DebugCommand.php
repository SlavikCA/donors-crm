<?php

namespace App\Console\Commands;

use App\Http\Debugger;
use Illuminate\Console\Command;

class DebugCommand extends Command
{
    protected $signature = 'debug';

    protected $description = 'Run debug command.';

    public function handle()
    {
        if (! class_exists(Debugger::class)) {
            $this->error('debug controller class not found!');

            return;
        }

        if (! app(Debugger::class)->handle($this)) {
            $this->error('failed.');

            return;
        }

        $this->info('success');
    }
}
