<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InstallAdminLte extends Command
{
    protected $signature = 'adminlte:assets';

    protected $description = 'Copy over adminlte assets';

    private string $publicPath;

    private string $vendorPath;

    public function handle()
    {
        $this->vendorPath = base_path('vendor/almasaeed2010/adminlte');
        $this->publicPath = public_path('adminlte');
        $this->mkdir($this->publicPath);
        $this->rmdirRecursive($this->publicPath);

        // copy over ALL assets
        $this->recursiveCopy("{$this->vendorPath}/plugins", "{$this->publicPath}/plugins");
        $this->recursiveCopy("{$this->vendorPath}/dist", "{$this->publicPath}/dist");

        //        $this->importFonts();
        //        $this->importCss();
        //        $this->importJs();
    }

    private function importFonts()
    {
        $fonts = [
            'plugins/fontawesome-free/webfonts/fa-solid-900.woff2',
            'plugins/fontawesome-free/webfonts/fa-solid-900.woff',
            'plugins/fontawesome-free/webfonts/fa-solid-900.ttf',
        ];

        foreach ($fonts as $style) {
            $this->copy("{$this->vendorPath}/$style", "{$this->publicPath}/$style");
        }
    }

    private function importCss()
    {
        $styles = [
            'plugins/fontawesome-free/css/all.min.css',
            'plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css',
            'plugins/icheck-bootstrap/icheck-bootstrap.min.css',
            'plugins/jqvmap/jqvmap.min.css',
            'dist/css/adminlte.min.css',
            'plugins/select2/css/select2.min.css',
            'plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css',
            'plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
            'plugins/daterangepicker/daterangepicker.css',
            'plugins/summernote/summernote-bs4.css',
        ];

        foreach ($styles as $style) {
            $this->copy("{$this->vendorPath}/$style", "{$this->publicPath}/$style");
        }
    }

    private function importJs()
    {
        $scripts = [
            'plugins/jquery/jquery.min.js',
            'plugins/jquery-ui/jquery-ui.min.js',
            'plugins/bootstrap/js/bootstrap.bundle.min.js',
            'plugins/select2/js/select2.full.min.js',
            'plugins/chart.js/Chart.min.js',
            'plugins/sparklines/sparkline.js',
            'plugins/jqvmap/jquery.vmap.min.js',
            'plugins/jqvmap/maps/jquery.vmap.usa.js',
            'plugins/jquery-knob/jquery.knob.min.js',
            'plugins/moment/moment.min.js',
            'plugins/daterangepicker/daterangepicker.js',
            'plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js',
            'plugins/summernote/summernote-bs4.min.js',
            'plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
            'dist/js/adminlte.js',
            'dist/js/pages/dashboard.js',
        ];

        foreach ($scripts as $script) {
            $this->copy("{$this->vendorPath}/$script", "{$this->publicPath}/$script");
        }
    }

    private function mkdir(string $path)
    {
        if (! is_dir($path)) {
            mkdir($path, 0755, true);
        }
    }

    private function copy(string $src, string $dest)
    {
        $basePath = dirname($dest);
        if (! is_dir($basePath)) {
            mkdir($basePath, 0755, true);
        }
        if (file_exists($dest)) {
            unlink($dest);
        }
        copy($src, $dest);
    }

    private function rmdirRecursive($dir)
    {
        foreach (scandir($dir) as $file) {
            if ($file === '.' || $file === '..') {
                continue;
            }
            if (is_dir("$dir/$file")) {
                $this->rmdirRecursive("$dir/$file");
            } else {
                unlink("$dir/$file");
            }
        }
        rmdir($dir);
    }

    /**
     * https://gist.github.com/gserrano/4c9648ec9eb293b9377b
     */
    public function recursiveCopy($src, $dst)
    {
        $dir = opendir($src);
        @mkdir($dst, 0755, true);
        while (($file = readdir($dir))) {
            if (($file != '.') && ($file != '..')) {
                if (is_dir($src.'/'.$file)) {
                    $this->recursiveCopy($src.'/'.$file, $dst.'/'.$file);
                } else {
                    copy($src.'/'.$file, $dst.'/'.$file);
                }
            }
        }
        closedir($dir);
    }
}
