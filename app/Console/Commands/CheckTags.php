<?php

namespace App\Console\Commands;

use App\Http\Services\DonorsService;
use Illuminate\Console\Command;

class CheckTags extends Command
{
    protected $signature = 'check:tags';

    protected $description = 'Check tags';

    public function handle()
    {
        /** @var DonorsService $service */
        $service = app(DonorsService::class);
        $this->info('checking recent donors...');
        $items = $service->checkDonorsRecentTag();
        $this->info("processed '$items' donors");

        $this->info('checking 18 month no contact donors...');
        $items = $service->checkDonorsNoContactIn18MonthTag();
        $this->info("processed '$items' donors");
    }
}
