<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description',
        'is_system',
    ];

    //    protected $appends = ['amount'];

    protected $casts = [
        'is_system' => 'boolean',
    ];

    public function getAmountAttribute()
    {
        return $this->donors()->count();
    }

    public function donors()
    {
        return $this->belongsToMany(Donor::class, 'donors_tags', 'tag_id', 'donor_id');
    }
}
