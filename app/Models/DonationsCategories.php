<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DonationsCategories extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'active',
    ];

    public function donation()
    {
        return $this->hasOne(Donation::class);
    }
}
