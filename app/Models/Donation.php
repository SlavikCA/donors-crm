<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Donation extends Model
{
    protected $fillable = [
        'donor_id',
        'date',
        'amount',
        'type',
        'category_id',
    ];

    protected $casts = [
        'date' => 'date',
    ];

    public function donor()
    {
        return $this->belongsTo(Donor::class);
    }

    public function category()
    {
        return $this->belongsTo(DonationsCategories::class);
    }
}
