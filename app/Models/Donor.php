<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Donor extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'id',
        'email',
        'phone',
        'name',
        'street',
        'city',
        'state',
        'zip',
    ];

    protected $appends = [
        'full_address',
    ];

    public function getFullAddressAttribute()
    {
        return "{$this->street} {$this->city} {$this->state}, {$this->zip}";
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function donations()
    {
        return $this->hasMany(Donation::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'donors_tags');
    }

    public function attributesToArray()
    {
        $array = parent::attributesToArray();
        $array['tags'] = $this->tags->map(function (Tag $tag) {
            $data = $tag->toArray();
            unset($data['pivot'], $data['is_system']);

            return $data;
        })->toArray();

        return $array;
    }
}
