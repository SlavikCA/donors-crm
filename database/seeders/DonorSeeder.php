<?php

namespace Database\Seeders;

use App\Models\Donor;
use Faker\Generator;
use Illuminate\Database\Seeder;

class DonorSeeder extends Seeder
{
    public function run()
    {
        /** @var Generator $faker */
        $faker = app(Generator::class);
        $donors = [
            [
                'id' => 1,
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'street' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->state,
                'zip' => $faker->postcode,
            ],
            [
                'id' => 2,
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'street' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->state,
                'zip' => $faker->postcode,
            ],
            [
                'id' => 3,
                'name' => $faker->name,
                'email' => $faker->email,
                'phone' => $faker->phoneNumber,
                'street' => $faker->streetAddress,
                'city' => $faker->city,
                'state' => $faker->state,
                'zip' => $faker->postcode,
            ],
        ];

        foreach ($donors as $donorData) {
            if (! $donor = Donor::find($donorData['id'])) {
                $donor = new Donor(['id' => $donorData['id']]);
            }
            $donor->fill($donorData)->save();
        }
    }
}
