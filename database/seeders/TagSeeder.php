<?php

namespace Database\Seeders;

use App\Models\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    public function run()
    {
        $tags = [
            'inactive' => [
                'description' => 'we don\'t send newsletter to inactive users',
                'is_system' => true,
            ],
            'Kochenkov' => 'church of Kochankov',
            'Chumakin' => 'church of Chumakin',
            'Avdeev' => 'church of Avdeev',
            'Slavik' => 'contacts of Slavik',
            'AIDCIM' => 'contacts of AIDCIM',
            'Hetman' => 'church of Hetman',
            'Iosif' => 'contacts of Iosif',
            'added in last 24hrs' => [
                'description' => 'added in last 24hrs',
                'is_system' => true,
            ],
            'no contacts in 18months' => [
                'description' => 'no contacts in 18months',
                'is_system' => true,
            ],
            'active donor' => [
                'description' => 'active donor',
                'is_system' => true,
            ],
            'Active PayPal' => 'Active PayPal',
            'Payeasy' => 'Active Payeasy',
            'Agafonov' => 'Bellingham church',
        ];

        foreach ($tags as $name => $details) {
            if (! $tag = Tag::whereName($name)->first()) {
                $tag = new Tag;
            }

            if (is_array($details)) {
                $tag->name = $name;
                $tag->description = $details['description'];
                $tag->is_system = $details['is_system'] ?? false;
                $tag->save();
            } else {
                $tag->name = $name;
                $tag->description = $details;
                $tag->is_system = false;
                $tag->save();
            }
        }
    }
}
