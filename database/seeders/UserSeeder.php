<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        /** @var Generator $faker */
        $faker = app(Generator::class);

        $users = [
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@email.com',
                'password' => Hash::make('password1'),
                'active' => true,
            ],
            [
                'id' => 2,
                'name' => 'Active User',
                'email' => $faker->unique()->safeEmail,
                'password' => Hash::make('password1'),
                'active' => true,
            ],
            [
                'id' => 3,
                'name' => 'Inactive User',
                'email' => $faker->unique()->safeEmail,
                'password' => Hash::make('password1'),
                'active' => false,
            ],
        ];

        foreach ($users as $userData) {
            if (! $user = User::find($userData['id'])) {
                $user = new User(['id' => $userData['id']]);
            }

            $user->email = $userData['email'];
            $user->name = $userData['name'];
            $user->password = $userData['password'];
            $user->active = $userData['active'];
            $user->save();
        }
    }
}
