<?php

namespace Database\Seeders;

use App\Models\Comment;
use App\Models\Tag;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DonorTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tagIds = [
            'Slavik' => Tag::whereName('Slavik')->first()->id,
            'Kochenkov' => Tag::whereName('Kochenkov')->first()->id,
            'Chumakin' => Tag::whereName('Chumakin')->first()->id,
            'Hetman' => Tag::whereName('Hetman')->first()->id,
            'last_24_hrs' => Tag::whereName('added in last 24hrs')->first()->id,
        ];
        $donorTags = [
            ['donor_id' => 1, 'tag_id' => $tagIds['Slavik']],
            ['donor_id' => 2, 'tag_id' => $tagIds['Slavik']],
            ['donor_id' => 1, 'tag_id' => $tagIds['Kochenkov']],
            ['donor_id' => 2, 'tag_id' => $tagIds['Chumakin']],
            ['donor_id' => 3, 'tag_id' => $tagIds['Kochenkov']],
            ['donor_id' => 1, 'tag_id' => $tagIds['Chumakin']],
            ['donor_id' => 3, 'tag_id' => $tagIds['Chumakin']],
            ['donor_id' => 1, 'tag_id' => $tagIds['Hetman']],
            ['donor_id' => 3, 'tag_id' => $tagIds['last_24_hrs']],
        ];

        foreach ($donorTags as $data) {
            if (! DB::table('donors_tags')->where($data)->count() > 0) {
                DB::table('donors_tags')->insert($data);
            }
        }

        $comments = [
            ['donor_id' => 1, 'comment' => 'User Admin made changes:<br>Changed tags to Slavik'],
            ['donor_id' => 2, 'comment' => 'User Admin made changes:<br>Changed tags to Slavik'],
            ['donor_id' => 1, 'comment' => 'User Admin made changes:<br>Changed tags to Kochenkov'],
            ['donor_id' => 2, 'comment' => 'User Admin made changes:<br>Changed tags to Chumakin'],
            ['donor_id' => 3, 'comment' => 'User Admin made changes:<br>Changed tags to Kochenkov'],
            ['donor_id' => 1, 'comment' => 'User Admin made changes:<br>Changed tags to Chumakin'],
            ['donor_id' => 3, 'comment' => 'User Admin made changes:<br>Changed tags to Chumakin'],
            ['donor_id' => 1, 'comment' => 'User Admin made changes:<br>Changed tags to Hetman'],
        ];

        foreach ($comments as $data) {
            if (! Comment::where($data)->count() > 0) {
                Comment::create($data);
            }
        }
    }
}
