<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        app(UserSeeder::class)->run();
        app(TagSeeder::class)->run();
        app(DonationCategorySeeder::class)->run();
        app(DonorSeeder::class)->run();
        app(DonationsSeeder::class)->run();
        app(DonorTagsSeeder::class)->run();
    }
}
