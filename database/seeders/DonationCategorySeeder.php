<?php

namespace Database\Seeders;

use App\Models\DonationsCategories;
use Illuminate\Database\Seeder;

class DonationCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'General Fund',
                'active' => true,
            ],
            [
                'name' => 'Shoe Fund',
                'active' => true,
            ],
            [
                'name' => 'Warm Winter',
                'active' => true,
            ],
            [
                'name' => 'Animal Fund',
                'active' => true,
            ],
            [
                'name' => 'Food Parcels',
                'active' => true,
            ],
            [
                'name' => 'School Supplies',
                'active' => true,
            ],
            [
                'name' => 'Basic Needs',
                'active' => true,
            ],
            [
                'name' => 'Medical Expenses',
                'active' => true,
            ],
            [
                'name' => 'Orphan Fund',
                'active' => true,
            ],
            [
                'name' => 'Funeral Fund',
                'active' => true,
            ],
            [
                'name' => 'Disaster Relief',
                'active' => true,
            ],
            [
                'name' => 'Missionary Fund',
                'active' => true,
            ],
            [
                'name' => 'Children Cloth',
                'active' => true,
            ],
        ];

        foreach ($categories as $categoryData) {
            if (! $category = DonationsCategories::whereName($categoryData['name'])->first()) {
                $category = new DonationsCategories;
            }

            $category->name = $categoryData['name'];
            $category->active = $categoryData['active'];
            $category->save();
        }
    }
}
