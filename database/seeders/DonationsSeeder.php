<?php

namespace Database\Seeders;

use App\Models\Donation;
use Illuminate\Database\Seeder;

class DonationsSeeder extends Seeder
{
    public function run()
    {
        $donations = require __DIR__.'/data/donations.php';

        foreach ($donations as $donationData) {
            if (! $donation = Donation::find($donationData['id'])) {
                $donation = new Donation(['id' => $donationData['id']]);
            }
            $donation->fill($donationData)->save();
        }
    }
}
