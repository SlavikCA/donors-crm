<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('donors', function (Blueprint $table) {
            $table->softDeletes();
        });

        $donors = DB::table('donors')->whereName('DELETED')->get(['id', 'updated_at']);
        foreach ($donors as $donor) {
            // was deleted @ last "update" timestamp
            DB::table('donors')->whereId($donor->id)->update(['deleted_at' => $donor->updated_at]);
        }
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('donors', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
};
