<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('donors_tags', function (Blueprint $table) {
            $table->integer('donor_id')->unsigned()->nullable()->change();
            $table->integer('tag_id')->unsigned()->nullable()->change();
        });

        Schema::table('donors', function (Blueprint $table) {
            $table->increments('id')->change();
        });

        Schema::table('tags', function (Blueprint $table) {
            $table->increments('id')->change();
        });

        Schema::table('donors_tags', function (Blueprint $table) {
            $table->foreign('donor_id')->references('id')->on('donors');
            $table->foreign('tag_id')->references('id')->on('tags');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('donors_tags', function (Blueprint $table) {
            $table->dropForeign('donors_tags_donor_id_foreign');
            $table->dropIndex('donors_tags_donor_id_foreign');
            $table->dropForeign('donors_tags_tag_id_foreign');
            $table->dropIndex('donors_tags_tag_id_foreign');
        });
    }
};
