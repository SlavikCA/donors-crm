<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('donors', function (Blueprint $table) {
            $table->index('email');
        });
        Schema::table('donors', function (Blueprint $table) {
            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        if (Schema::getConnection() instanceof \Illuminate\Database\SQLiteConnection) {
            return;
        }

        Schema::table('donors', function (Blueprint $table) {
            $table->dropIndex(['email']);
            $table->dropIndex(['name']);
        });
    }
};
