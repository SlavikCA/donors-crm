<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up()
    {
        Schema::table('donations', function (Blueprint $table) {
            $table->integer('donor_id')->unsigned()->nullable()->change();
            $table->integer('category_id')->unsigned()->nullable()->change();
        });

        Schema::table('donations_categories', function (Blueprint $table) {
            $table->increments('id')->change();
        });

        Schema::table('donations', function (Blueprint $table) {
            $table->foreign('donor_id')->references('id')->on('donors');
            $table->foreign('category_id')->references('id')->on('donations_categories');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down()
    {
        Schema::table('donations', function (Blueprint $table) {
            $table->dropForeign('donations_donor_id_foreign');
            $table->dropIndex('donations_donor_id_foreign');
            $table->dropForeign('donations_category_id_foreign');
            $table->dropIndex('donations_category_id_foreign');
        });
    }
};
