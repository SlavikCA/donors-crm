#!/usr/bin/env sh
set -x
pwd
whoami
ls -la
composer install --optimize-autoloader
npm install && npm run build
php artisan adminlte:assets
php artisan config:clear
php artisan cache:clear
php -r 'opcache_reset();'
php artisan migrate --force
php artisan route:clear
php artisan config:cache
php artisan view:clear

# force flush of app opcache
curl -i -X GET http://localhost/api/reset-opcache

chmod 777 -R storage/framework/*
chmod 777 -R storage/logs
