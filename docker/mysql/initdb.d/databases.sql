# create databases
CREATE DATABASE IF NOT EXISTS `local_api`;

# create user and grant rights
CREATE USER 'local_user'@'127.0.0.1' IDENTIFIED BY 'local_password';
GRANT ALL ON local_api.* TO 'local_user'@'127.0.0.1';
