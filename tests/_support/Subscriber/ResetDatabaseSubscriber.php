<?php

namespace Tests\Support\Subscriber;

use Dotenv\Dotenv;
use Exception;
use PHPUnit\Event\Test\PreparationStarted;
use PHPUnit\Event\Test\PreparationStartedSubscriber;

class ResetDatabaseSubscriber implements PreparationStartedSubscriber
{
    private ?string $baseClass = null;

    public function notify(PreparationStarted $event): void
    {
        $className = strtok(substr($event->test()->file(), strrpos($event->test()->file(), '/') + 1), '.');

        if ($this->baseClass !== $className) {
            $this->setupDb();
            $this->baseClass = $className;
        }
    }

    private function setupDb()
    {
        $rootPath = realpath(__DIR__.'/../../../');

        if (file_exists("$rootPath/.env.testing")) {
            if (! empty(trim(file_get_contents("$rootPath/.env.testing")))) {
                $dotenv = Dotenv::createMutable($rootPath, '.env.testing');
                $dotenv->load();
            }
        }

        $connection = $_ENV['DB_CONNECTION'] ?? $_SERVER['DB_CONNECTION'] ?? 'local';

        $code = $this->runCommand("php artisan migrate:fresh --database=$connection --force --seed");

        if ($code === 0) {
            return;
        }

        throw new Exception('could not setup database');
    }

    private function runCommand(string $command): int
    {
        $result = trim(shell_exec("$command; echo $?"));

        return (int) substr($result, strrpos($result, "\n"));
    }
}
