<?php

namespace Tests\Feature;

use App\Models\User;
use Faker\Factory;
use Faker\Generator;
use Illuminate\Support\Str;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    protected Generator $faker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
    }

    public function test_can_see_register_page()
    {
        $response = $this->get('/register');
        $response->assertSuccessful();
        $response->assertViewIs('auth.register');

        $response->assertSeeText('Register');
        $response->assertSeeText('Name');
        $response->assertSeeText('E-Mail Address');
        $response->assertSeeText('Password');
        $response->assertSeeText('Confirm Password');
    }

    public function test_can_register_invalid_name()
    {
        $password = Str::random();
        $response = $this->post('/register', [
            'name' => '',
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => $password,
        ]);
        $response->assertSessionHasErrors(['name' => 'The name field is required.']);
    }

    public function test_can_register_invalid_email()
    {
        $password = Str::random();
        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'email' => '',
            'password' => $password,
            'password_confirmation' => $password,
        ]);
        $response->assertSessionHasErrors(['email' => 'The email field is required.']);

        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'email' => 'bananas',
            'password' => $password,
            'password_confirmation' => $password,
        ]);
        $response->assertSessionHasErrors(['email' => 'The email must be a valid email address.']);
    }

    public function test_can_register_invalid_password()
    {
        $password = Str::random();
        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => '',
            'password_confirmation' => $password,
        ]);
        $response->assertSessionHasErrors(['password' => 'The password field is required.']);
    }

    public function test_can_register_invalid_password_confirmation()
    {
        $password = Str::random();
        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => '',
        ]);
        $response->assertSessionHasErrors(['password' => 'The password confirmation does not match.']);

        $response = $this->post('/register', [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => $password,
            'password_confirmation' => Str::random(),
        ]);
        $response->assertSessionHasErrors(['password' => 'The password confirmation does not match.']);
    }

    public function test_can_register_successfully()
    {
        $data = [
            'name' => $this->faker->name,
            'email' => 'new_user_registration@email.com',
            'password' => 'password1',
            'password_confirmation' => 'password1',
        ];
        $response = $this->post('/register', $data);

        $user = User::whereEmail($data['email'])->first();

        $response->assertRedirect('/dashboard/donations');
        $this->assertAuthenticatedAs($user);
    }

    public function test_inactive_user_denied()
    {
        $user = User::whereEmail('new_user_registration@email.com')->first();

        $response = $this->actingAs($user)->followingRedirects()->get('/dashboard/donations');
        $response->assertSeeText('Account is not active.');
        $this->assertGuest();
    }
}
