<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function test_can_see_login_page()
    {
        $response = $this->get('/login');
        $response->assertSuccessful();
        $response->assertViewIs('auth.login');

        $response->assertSeeText('Login');
        $response->assertSeeText('E-Mail Address');
        $response->assertSeeText('Password');
    }

    public function test_can_login_successfully()
    {
        $user = User::find(1);

        $response = $this->post('/login', [
            'email' => 'admin@email.com',
            'password' => 'password1',
        ]);

        $response->assertRedirect('/dashboard/donations');
        $this->assertAuthenticatedAs($user);
    }

    public function test_can_login_invalid_password()
    {
        $response = $this->post('/login', [
            'email' => 'admin@email.com',
            'password' => 'invalid-password',
        ]);
        $response->assertSessionHasErrors(['email' => 'These credentials do not match our records.']);
    }
}
